//
//  AppDelegate.m
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AppDelegate.h"
#import "StringTable.h"
#import "Message.h"
#import "ChurchAPIClient.h"
#import "Church.h"
#import "StringTable.h"
#import "Utility.h"
#import "ServiceTime.h"
#import <Crashlytics/Crashlytics.h>
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"be5731fac922d29f3b102c5b27f6f4f36e8991f0"];
    // Override point for customization after application launch.
    [self setupDB];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self syncChurch];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) setupDB
{
    NSLog(@"set up!!");
    [MagicalRecord setupCoreDataStackWithStoreNamed:APP_DB];
}

- (void) syncChurch{
    [[ChurchAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",CHURCH_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncChurch successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
    
        NSArray * arrChurch = (NSArray *)myDict;
        NSLog(@"syncChurch count %d",[arrChurch count]);
        for(NSInteger i=0;i<[arrChurch count];i++){
            NSDictionary * dicEmp = (NSDictionary *)[arrChurch objectAtIndex:i];
            NSInteger idx = [[dicEmp objectForKey:@"id"] integerValue];
            NSString * strIdx = [NSString stringWithFormat:@"%d",idx];
            NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strIdx];
            
            NSString * address = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"address"] shouldCleanWhiteSpace:YES];
            NSString * name = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"name"] shouldCleanWhiteSpace:YES];
            NSString * phone = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"telephone"] shouldCleanWhiteSpace:YES];
            NSString * email = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"email"] shouldCleanWhiteSpace:YES];
            NSLog(@"church email %@",email);
            CGFloat lat=0.0;
            CGFloat lon = 0.0;
            if ([dicEmp objectForKey:@"latitude"]  != [NSNull null] && [dicEmp objectForKey:@"longitude"]  != [NSNull null]) {
                lat = [[dicEmp objectForKey:@"latitude"] floatValue];
                lon = [[dicEmp objectForKey:@"longitude"] floatValue];
            }
            
            Church * obj = [Church MR_findFirstWithPredicate:predicate inContext:localContext];
            if (obj != nil) {
                obj.idx = strIdx;
                obj.name = name;
                obj.address = address;
                obj.email = email;
                obj.telephone = phone;
                obj.lat = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",lat]];
                obj.lon = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",lon]];
                [self updateChurch:obj withLocalContext:localContext];
            }
            else{
                Church * objChurch = [Church MR_createEntity];
                objChurch.idx = strIdx;
                objChurch.name = name;
                objChurch.address = address;
                objChurch.email = email;
                objChurch.telephone = phone;
                objChurch.lat = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",lat]];
                objChurch.lon = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",lon]];
                [self insertChurch:objChurch withLocalContext:localContext];
            }
            NSDictionary * serviceTime = [dicEmp objectForKey:@"service_times"];
            NSArray * arrMonday = [serviceTime objectForKey:SERVICE_MONDAY];
            NSArray * arrTuesday = [serviceTime objectForKey:SERVICE_TUESDAY];
            NSArray * arrWed = [serviceTime objectForKey:SERVICE_WEDNESDAY];
            NSArray * arrThur = [serviceTime objectForKey:SERVICE_THURSDAY];
            NSArray * arrFri = [serviceTime objectForKey:SERVICE_FRIDAY];
            NSArray * arrSat = [serviceTime objectForKey:SERVICE_SATURDAY];
            NSArray * arrSun = [serviceTime objectForKey:SERVICE_SUNDAY];
            [self getServiceFromServicArr:arrMonday];
            [self getServiceFromServicArr:arrTuesday];
            [self getServiceFromServicArr:arrWed];
            [self getServiceFromServicArr:arrThur];
            [self getServiceFromServicArr:arrFri];
            [self getServiceFromServicArr:arrSat];
            [self getServiceFromServicArr:arrSun];
        }
        [self syncMessage];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
       
    }];
    
}

- (void) getServiceFromServicArr:(NSArray *)arr{
    for(NSInteger i=0;i<[arr count];i++){
        NSDictionary * dicService = (NSDictionary *)[arr objectAtIndex:i];
        [self insertUpdateServiceTime:dicService];
    }
}

- (void) insertUpdateServiceTime:(NSDictionary *)dic{
    NSString * created_at = [Utility checkStringNotToBeNull:[dic objectForKey:@"created_at"] shouldCleanWhiteSpace:YES];
    NSString * day = [Utility checkStringNotToBeNull:[dic objectForKey:@"day"] shouldCleanWhiteSpace:YES];
    NSString * end_am_pm = [Utility checkStringNotToBeNull:[dic objectForKey:@"end_am_pm"] shouldCleanWhiteSpace:YES];
    NSString * end_minute = [Utility checkStringNotToBeNull:[dic objectForKey:@"end_minute"] shouldCleanWhiteSpace:YES];
    NSString * start_am_pm = [Utility checkStringNotToBeNull:[dic objectForKey:@"start_am_pm"] shouldCleanWhiteSpace:YES];
    NSString * start_minute = [Utility checkStringNotToBeNull:[dic objectForKey:@"start_minute"] shouldCleanWhiteSpace:YES];
     NSString * updated_at = [Utility checkStringNotToBeNull:[dic objectForKey:@"updated_at"] shouldCleanWhiteSpace:YES];
    NSInteger idx = [[dic objectForKey:@"id"] integerValue];
    NSInteger church_id = [[dic objectForKey:@"church_id"] integerValue];
    NSInteger end_hour = [[dic objectForKey:@"end_hour"] integerValue];
    NSInteger start_hour = [[dic objectForKey:@"start_hour"] integerValue];
    NSString * strIdx = [NSString stringWithFormat:@"%d",idx];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
    NSInteger start_timetick = (start_hour * 60) + [start_minute integerValue];
    if ([start_am_pm isEqualToString:@"PM"]) {
        start_timetick += (12*60);
        NSLog(@"need to add for pm!!! and time start time tick %d",start_timetick);
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strIdx];
    ServiceTime * obj = [ServiceTime MR_findFirstWithPredicate:predicate inContext:localContext];
    if (obj != nil) {
        obj.idx = strIdx;
        obj.church_id = [NSString stringWithFormat:@"%d",church_id];
        obj.day = day;
        obj.end_am_pm = end_am_pm;
        obj.end_minute = end_minute;
        obj.end_hour = [NSNumber numberWithInt:end_hour];
        obj.start_am_pm = start_am_pm;
        obj.start_minute = start_minute;
        obj.start_hour = [NSNumber numberWithInt:start_hour];
        obj.start_hour_timetick = [NSNumber numberWithInt:start_timetick];
        obj.created_at = created_at;
        obj.updated_at = updated_at;
        //[obj setStartHourTimetick];
        //[obj setEndHourTimetick];
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            //Church * objChurch = [Church MR_findFirstByAttribute:@"idx" withValue:obj.server_id inContext:localContext];
        }];
    }
    else{
        ServiceTime * obj2 = [ServiceTime MR_createEntity];
        obj2.idx = strIdx;
        obj2.church_id = [NSString stringWithFormat:@"%d",church_id];
        obj2.day = day;
        obj2.end_am_pm = end_am_pm;
        obj2.end_minute = end_minute;
        obj2.end_hour = [NSNumber numberWithInt:end_hour];
        obj2.start_am_pm = start_am_pm;
        obj2.start_minute = start_minute;
        obj2.start_hour = [NSNumber numberWithInt:start_hour];
        obj2.start_hour_timetick = [NSNumber numberWithInt:start_timetick];
        obj2.created_at = created_at;
        obj2.updated_at = updated_at;
        //[obj2 setStartHourTimetick];
        //[obj2 setEndHourTimetick];
        [obj2 MR_inContext:localContext];
        // Save the modification in the local context
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            NSLog(@"service time saved");
        }];
    }
}

- (void) insertChurch:(Church *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    [obj MR_inContext:localContext];
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Church saved");
    }];
}

- (void) updateChurch:(Church *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx ==[c] %@ ", obj.idx];
    Church * churchFound = [Church MR_findFirstWithPredicate:predicate inContext:localContext];
    if (churchFound) {
        churchFound = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            //Church * objChurch = [Church MR_findFirstByAttribute:@"idx" withValue:obj.server_id inContext:localContext];
        }];
    }
}

- (void)syncMessage{
    [[ChurchAPIClient sharedClient] GET:[NSString stringWithFormat:@"%@",MESSAGE_LINK] parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncMessage successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arrChurch = (NSArray *)myDict;
        NSLog(@"syncMessage count %d",[arrChurch count]);
        for(NSInteger i=0;i<[arrChurch count];i++){
            NSDictionary * dicEmp = (NSDictionary *)[arrChurch objectAtIndex:i];
            NSInteger idx = [[dicEmp objectForKey:@"id"] integerValue];
            NSString * strIdx = [NSString stringWithFormat:@"%d",idx];
            NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strIdx];
            
            NSString * detail_link = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"detail_link"] shouldCleanWhiteSpace:YES];
            NSString * title_image_link = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"title_image"] shouldCleanWhiteSpace:YES];
            NSString * detail_image_link = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"detail_image"] shouldCleanWhiteSpace:YES];
            NSString * title = [Utility checkStringNotToBeNull:[dicEmp objectForKey:@"title"] shouldCleanWhiteSpace:YES];
            NSInteger animation_type = [[dicEmp objectForKey:@"animation_type"] integerValue];
            NSInteger order_id = [[dicEmp objectForKey:@"order_number"] integerValue];
            
            Message * obj = [Message MR_findFirstWithPredicate:predicate inContext:localContext];
            if (obj != nil) {
                obj.idx = strIdx;
                obj.detail = detail_link;
                obj.image_url = title_image_link;
                obj.tittle = title;
                obj.detail_image = detail_image_link;
                obj.animation_type = [NSNumber numberWithInteger:animation_type];
                obj.order_id = [NSNumber numberWithInteger:order_id];
                [self updateMessage:obj withLocalContext:localContext];
            }
            else{
                Message * objMessage = [Message MR_createEntity];
                objMessage.idx = strIdx;
                objMessage.detail = detail_link;
                objMessage.image_url = title_image_link;
                objMessage.tittle = title;
                objMessage.detail_image = detail_image_link;
                objMessage.order_id = [NSNumber numberWithInteger:order_id];
                objMessage.animation_type = [NSNumber numberWithInteger:animation_type];
                [self insertMessage:objMessage withLocalContext:localContext];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMessage" object:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        
    }];
}

- (void) insertMessage:(Message *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    [obj MR_inContext:localContext];
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Message saved");
    }];
}

- (void) updateMessage:(Message *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx ==[c] %@ ", obj.idx];
    Message * churchFound = [Message MR_findFirstWithPredicate:predicate inContext:localContext];
    if (churchFound) {
        churchFound = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            //Church * objChurch = [Church MR_findFirstByAttribute:@"idx" withValue:obj.server_id inContext:localContext];
        }];
    }
}

@end
