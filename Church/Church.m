//
//  Church.m
//  Church
//
//  Created by Zayar on 5/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Church.h"


@implementation Church

@dynamic address;
@dynamic email;
@dynamic idx;
@dynamic lat;
@dynamic lon;
@dynamic name;
@dynamic telephone;
@dynamic distance;

@end
