//
//  CustomSegmentControl.m
//  Church
//
//  Created by Zayar on 5/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "CustomSegmentControl.h"
@interface CustomSegmentControl()
{
    NSInteger current;
}
@end
@implementation CustomSegmentControl
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    current = self.selectedSegmentIndex;
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    if (current == self.selectedSegmentIndex)
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    NSLog(@"current index %d",current);
}

@end
