//
//  CircleView.m
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    /* Set UIView Border */
    /*
     // Get the contextRef
     CGContextRef contextRef = UIGraphicsGetCurrentContext();
     
     // Set the border width
     CGContextSetLineWidth(contextRef, 5.0);
     
     // Set the border color to RED
     CGContextSetRGBStrokeColor(contextRef, 255.0, 0.0, 0.0, 1.0);
     
     // Draw the border along the view edge
     CGContextStrokeRect(contextRef, rect);
     */
    
    /* Draw a circle */
    // Get the contextRef
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // Set the border width
    CGContextSetLineWidth(contextRef, 1.0);
    
    // Set the circle fill color to GREEN
    CGContextSetRGBFillColor(contextRef, 249, 249, 251, 1.0);
    
    // Set the cicle border color to BLUE
    CGContextSetRGBStrokeColor(contextRef, 249, 249, 251, 1.0);
    
    // Fill the circle with the fill color
    CGContextFillEllipseInRect(contextRef, rect);
    
    // Draw the circle border
    CGContextStrokeEllipseInRect(contextRef, rect);
}

@end
