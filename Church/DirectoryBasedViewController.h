//
//  DirectoryBasedViewController.h
//  Church
//
//  Created by Zayar on 5/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
@interface DirectoryBasedViewController : UIViewController
@property (nonatomic,strong) HeaderView * headerView;
@end
