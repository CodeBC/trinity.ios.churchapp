//
//  DirectoryViewController.h
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
@protocol DirectoryViewControllerDelegate;
@interface DirectoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic)id<DirectoryViewControllerDelegate> owner;
@property (nonatomic, strong) HeaderView * headerView;
- (void) reLoadTheView;
@end
@protocol DirectoryViewControllerDelegate
- (void) onHeaderViewTap:(DirectoryViewController *)viewController andHeaderView:(HeaderView *)headerView;
@end
