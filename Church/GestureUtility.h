//
//  GestureUtility.h
//  ThePresidentOffice
//
//  Created by Zayar on 1/16/13.
//
//

#import <Foundation/Foundation.h>

@interface GestureUtility : NSObject

+(void)setDragForViewX:(UIView *)view;
+(void)dragViewX:(UIPanGestureRecognizer *)sender;
+(void)setDragForViewX:(UIView *)view withBeyondView:(UIView *)bView;
+(void)setDragForViewY:(UIView *)view withBeyondView:(UIView *)bView;
+(void)dragViewY:(UIPanGestureRecognizer *)sender;
@end
