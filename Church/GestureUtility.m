//
//  GestureUtility.m
//  ThePresidentOffice
//
//  Created by Zayar on 1/16/13.
//
//

#import "GestureUtility.h"
#import "StringTable.h"
UIView * bePView;
@implementation GestureUtility

+(void)setDragForViewX:(UIView *)view
{
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewX:)];
    [view addGestureRecognizer:panGestureRecognizer];
}

+(void)setDragForViewX:(UIView *)view withBeyondView:(UIView *)bView
{
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewX:)];
    [view addGestureRecognizer:panGestureRecognizer];
    bePView = bView;
}

+(void)dragViewX:(UIPanGestureRecognizer *)sender
{
    UIView *targetView;
    
        targetView = sender.view;
        CGPoint p = [sender translationInView:targetView];
        
        //CGPoint movedPoint = CGPointMake(targetView.center.x + p.x, targetView.center.y + p.y);
        CGPoint movedPoint;
        CGPoint bViewMovedPoint;
        if ((targetView.center.x + p.x) > (targetView.frame.size.width /2) & (targetView.center.x + p.x) < 300) {
            movedPoint = CGPointMake(targetView.center.x + p.x, targetView.center.y);
            targetView.center = movedPoint;
            bViewMovedPoint = CGPointMake(bePView.center.x + p.x, bePView.center.y);
            bePView.center = bViewMovedPoint;
            /*if ((targetView.center.x + p.x) > 100) {
             [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.) + 200, targetView.center.y);}];
             }
             else{
             [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.), targetView.center.y);}];
             }*/
            
            if (sender.state == UIGestureRecognizerStateEnded){
                if ((targetView.center.x + p.x) > 200) {
                    [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.) + 300, targetView.center.y);}];
                    [UIView animateWithDuration:.3 animations:^{bePView.center = CGPointMake(roundf(targetView.bounds.origin.x + bePView.bounds.size.width/2 + 10) , bePView.center.y);}];
                }
                else{
                    [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.), targetView.center.y);}];
                    [UIView animateWithDuration:.3 animations:^{bePView.center = CGPointMake(roundf(targetView.bounds.origin.x - bePView.bounds.size.width/2 - 10), bePView.center.y);}];
                }
                
            }
            
        }
     
        [sender setTranslation:CGPointZero inView:targetView];
    
    //[UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.) + 200, targetView.center.y);}];
}

+(void)setDragForViewY:(UIView *)view withBeyondView:(UIView *)bView
{
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragViewY:)];
    [view addGestureRecognizer:panGestureRecognizer];
    bePView = bView;
}

+(void)dragViewY:(UIPanGestureRecognizer *)sender
{
    UIView *targetView;
    
    targetView = sender.view;
    CGPoint p = [sender translationInView:targetView];
    CGPoint movedPoint;
    CGPoint bViewMovedPoint;
    movedPoint = CGPointMake(targetView.center.x , targetView.center.y+ p.y);
    targetView.center = movedPoint;
    NSLog(@"pan gesture point (x:%f,y:%f) plus targetview(x:%f,y:%f)",p.x,p.y,targetView.center.x,targetView.center.y);
    
    [sender setTranslation:CGPointZero inView:targetView];
    if (sender.state == UIGestureRecognizerStateEnded){
        
        if ((targetView.center.y) > 400) {
            NSLog(@"stage reached!!");
            [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(targetView.center.x, targetView.frame.size.width*2+220);}];
//            POPDecayAnimation *anim = [POPDecayAnimation animationWithPropertyNamed:kPOPLayerPositionY];
//            anim.velocity = @([sender velocityInView:targetView].y);
//            anim.deceleration = 0.99;
//            anim.toValue = @(targetView.frame.size.width*2+220);
//            [targetView pop_addAnimation:anim forKey:@"slide"];
        }
        else{
            NSLog(@"stage not reached!!");
            [UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(targetView.center.x, targetView.frame.size.width-36);}];
        }
    }
    //[UIView animateWithDuration:.3 animations:^{targetView.center = CGPointMake(roundf(targetView.bounds.size.width/2.) + 200, targetView.center.y);}];
}
@end
