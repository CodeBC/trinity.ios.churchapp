//
//  HeaderView.h
//  Church
//
//  Created by Zayar on 4/30/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HeaderViewDelegate;
@interface HeaderView : UIView
@property(nonatomic) id<HeaderViewDelegate> owner;
- (void) nameViewStyleSetup:(UIFont *)font andTextColor:(UIColor *)color;
- (void) showFadeInView;
- (void) showFadeOutView;
- (void) setNameText:(NSString *)strName;
- (void) arrowAnimationForDetail:(BOOL)open;
- (void) showSearchButton:(BOOL)show;
@end
@protocol HeaderViewDelegate 
- (void) onHeaderTapToClose:(HeaderView *)headerView;
- (void) onHeaderSearchClicked:(HeaderView *)headerView;
@end
