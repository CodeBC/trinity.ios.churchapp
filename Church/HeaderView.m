//
//  HeaderView.m
//  Church
//
//  Created by Zayar on 4/30/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "HeaderView.h"
#import "StringTable.h"
@interface HeaderView()
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UIImageView * arrowImageView;
@property (nonatomic,strong) UIImageView * imgBgView;
@property (nonatomic,strong) UIButton * btnSearch;
@property (nonatomic,strong) UITapGestureRecognizer *singleFingerTap;
@end
@implementation HeaderView
@synthesize owner;
- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(60, 24, 220, 71)];
        _lblName.backgroundColor = [UIColor clearColor];
        _lblName.numberOfLines = 2;
        [_lblName setAlpha:0.0];
    }
    return _lblName;
}

- (UIImageView *) arrowImageView{
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 38, 30, 30)];
        [_arrowImageView setBackgroundColor:[UIColor clearColor]];
        [_arrowImageView setImage:[UIImage imageNamed:@"ArrowIndicator"]];
        [_arrowImageView setAlpha:0.0];
    }
    return _arrowImageView;
}

- (UIButton *) btnSearch{
    if (!_btnSearch) {
        _btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnSearch.frame = CGRectMake(self.lblName.frame.origin.x + self.lblName.frame.size.width - 2, 38, 40, 40);
        [_btnSearch setBackgroundColor:[UIColor clearColor]];
        [_btnSearch setImage:[UIImage imageNamed:@"search_icon_white"] forState:normal];
        [_btnSearch setAlpha:0.0];
        [_btnSearch addTarget:self action:@selector(onSearch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnSearch;
}
- (UIImageView *) imgBgView{
    if (!_imgBgView) {
        _imgBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [_imgBgView setBackgroundColor:[UIColor clearColor]];
        [_imgBgView setImage:[UIImage imageNamed:@"header_bg"]];
        [_imgBgView setAlpha:0.0];
    }
    return _imgBgView;
}

- (UITapGestureRecognizer *) singleFingerTap{
    if (!_singleFingerTap) {
        _singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                   action:@selector(handleSingleTap:)];
        //
    }
    return _singleFingerTap;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self addSubview:self.imgBgView];
        [self addSubview:self.arrowImageView];
        [self addSubview:self.lblName];
        [self addSubview:self.btnSearch];
        [self setBackgroundColor:[UIColor clearColor]];
        [self addGestureRecognizer:self.singleFingerTap];
    }
    return self;
}

- (void) nameViewStyleSetup:(UIFont *)font andTextColor:(UIColor *)color{
    self.lblName.font = font;
    self.lblName.textColor = color;
}

- (void) setNameText:(NSString *)strName{
    self.lblName.text = strName;
}

- (void) showFadeInView{
    self.lblName.alpha = 0.0;
    [UIView animateWithDuration:2 animations:^{
        self.lblName.alpha = 1.0;
        self.arrowImageView.alpha = 1.0;
        self.btnSearch.alpha = 1.0;
        self.imgBgView.alpha = 1.0;
    }];
}

- (void) showFadeOutView{
    self.lblName.alpha = 1.0;
    [UIView animateWithDuration:1 animations:^{
        self.lblName.alpha = 0.0;
        self.arrowImageView.alpha = 0.0;
        self.btnSearch.alpha = 0.0;
        self.imgBgView.alpha = 0.0;
    }];
}

- (void) showSearchButton:(BOOL)show{
    if (show) {
        self.btnSearch.hidden = FALSE;
    }
    else{
        self.btnSearch.hidden = TRUE;
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *) gestureRecognizer{
    [owner onHeaderTapToClose:self];
}

-(void)arrowAnimationForDetail:(BOOL)open{
    if (open) {
        [UIView animateWithDuration:0.3 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(-3.14159265358979323846264338327950288/2);
        }];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            self.arrowImageView.transform = CGAffineTransformMakeRotation(0);
        }];
    }
}

- (void) onSearch:(UIButton *)sender{
    [owner onHeaderSearchClicked:self];
}

@end
