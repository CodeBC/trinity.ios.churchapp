//
//  ViewController.h
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
@interface MenuViewController : BasedViewController<HeaderViewDelegate>

@end
