//
//  ViewController.m
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MenuViewController.h"
#import "StringTable.h"
#import "DirectoryViewController.h"
#import "AboutViewController.h"
#import "GospelViewController.h"
#import "HeaderView.h"
#import "GestureUtility.h"
#import "FBShimmeringView.h"
#import "Utility.h"
@interface MenuViewController ()
{
    BOOL isContainerPresent;
}
@property (nonatomic,strong) UIButton * btnDirectory;
@property (nonatomic,strong) UIButton * btnGospel;
@property (nonatomic,strong) UIButton * btnAbout;
@property (nonatomic,strong) UIView * containerView;
@property (nonatomic,strong) UIView * aboutContainerView;
@property (nonatomic,strong) UIView * gosContainerView;
@property (nonatomic,strong) DirectoryViewController * directoryViewController;
@property (nonatomic,strong) AboutViewController * aboutViewController;
@property (nonatomic,strong) GospelViewController * gospelViewController;
@property (nonatomic,strong) UINavigationController * navDirectoryViewController;
@property (nonatomic,strong) HeaderView * headerView;
@property (nonatomic,strong) UIImageView * imgVline1;
@property (nonatomic,strong) UIImageView * imgVline2;

@property (nonatomic,strong) UIButton * btnShare;
@end

@implementation MenuViewController

#pragma mark init
- (UIButton *) btnShare{
    if (!_btnShare) {
        _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnShare setFrame:CGRectMake(290, self.view.frame.size.height-40, 21, 28)];
        [_btnShare setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
        [_btnShare addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnShare;
}

- (UIButton *) btnDirectory{
    if (!_btnDirectory) {
        _btnDirectory = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnDirectory setFrame:CGRectMake(55, self.view.frame.origin.y - 168, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
        [_btnDirectory setTitle:@"DIRECTORY" forState:normal];
        [_btnDirectory setBackgroundColor:[UIColor clearColor]];
        [_btnDirectory setTitleColor:[UIColor colorWithHexString:@"0ea4ee"] forState:normal];
        [_btnDirectory addTarget:self action:@selector(onDirectory:) forControlEvents:UIControlEventTouchUpInside];
        _btnDirectory.titleLabel.font = [UIFont fontWithName:BLANCE_CONDENSED size:72];
        [self registerEffectForView:_btnDirectory depth:25];
    }
    return _btnDirectory;
}

- (UIButton *) btnGospel{
    if (!_btnGospel) {
        _btnGospel = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnGospel setFrame:CGRectMake(self.view.frame.origin.x - MENU_BTN_WIDTH, 232,  MENU_BTN_WIDTH, 90)];
        [_btnGospel setTitle:@"GOSPEL" forState:normal];
        [_btnGospel setBackgroundColor:[UIColor clearColor]];
        //[_btnGospel setBackgroundColor:[UIColor redColor]];
        [_btnGospel setTitleColor:[UIColor colorWithHexString:@"000000"] forState:normal];
        _btnGospel.titleLabel.font = [UIFont fontWithName:BLANCE_CAPS_INLINE size:110];
        [_btnGospel addTarget:self action:@selector(onGospel:) forControlEvents:UIControlEventTouchUpInside];
        [self registerEffectForView:_btnGospel depth:25];
    }
    return _btnGospel;
}

- (UIButton *) btnAbout{
    if (!_btnAbout) {
        _btnAbout = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnAbout setFrame:CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+MENU_BTN_WIDTH, 400, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
        [_btnAbout setTitle:@"ABOUT" forState:normal];
        [_btnAbout setBackgroundColor:[UIColor clearColor]];
        //[_btnAbout setBackgroundColor:[UIColor redColor]];
        [_btnAbout setTitleColor:[UIColor colorWithHexString:@"0ea4ee"] forState:normal];
        [_btnAbout addTarget:self action:@selector(onAbout:) forControlEvents:UIControlEventTouchUpInside];
        _btnAbout.titleLabel.font = [UIFont fontWithName:BLANCE_CONDENSED size:72];
        [self registerEffectForView:_btnAbout depth:25];
    }
    return _btnAbout;
}

- (UIImageView *) imgVline1{
    if (!_imgVline1) {
        _imgVline1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line"]];
        [_imgVline1 setFrame:CGRectMake(60,225, 0, 0)];
        [self registerEffectForView:_imgVline1 depth:25];
    }
    return _imgVline1;
}

- (UIImageView *) imgVline2{
    if (!_imgVline2) {
        _imgVline2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line"]];
        [_imgVline2 setFrame:CGRectMake(252,330, 0, 0)];
        [self registerEffectForView:_imgVline2 depth:25];
    }
    return _imgVline2;
}

- (UIView *) containerView{
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height- HEADER_GAP_HEIGHT)];
        [_containerView setBackgroundColor:[UIColor colorWithHexString:@"000000"]];
        _containerView.hidden = YES;
        [_containerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    }
    return _containerView;
}

- (UIView *) gosContainerView{
    if (!_gosContainerView) {
        _gosContainerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [_gosContainerView setBackgroundColor:[UIColor colorWithHexString:@"000000"]];
        _gosContainerView.hidden = YES;
    }
    return _gosContainerView;
}

- (UIView *) aboutContainerView{
    if (!_aboutContainerView) {
        _aboutContainerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [_aboutContainerView setBackgroundColor:[UIColor colorWithHexString:@"000000"]];
        _aboutContainerView.hidden = YES;
    }
    return _aboutContainerView;
}

- (UINavigationController *) navDirectoryViewController{
    if (!_navDirectoryViewController) {
        _navDirectoryViewController = [[UIStoryboard storyboardWithName:@"DirectoryStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NavDirectory"];
    }
    return _navDirectoryViewController;
}

- (DirectoryViewController *) directoryViewController{
    if (!_directoryViewController) {
       
        if ([self.navDirectoryViewController.viewControllers count]) {
            _directoryViewController = self.navDirectoryViewController.viewControllers[0];
            _directoryViewController.owner = self;
            
        }
        else{
             _directoryViewController = [[UIStoryboard storyboardWithName:@"DirectoryStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"Directory"];
            _directoryViewController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
            
        }
    }
    return _directoryViewController;
}

- (AboutViewController *) aboutViewController{
    if (!_aboutViewController) {
        _aboutViewController = [[UIStoryboard storyboardWithName:@"AboutStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"About"];
        _aboutViewController.view.frame = CGRectMake(0, 0, self.aboutContainerView.frame.size.width, self.aboutContainerView.frame.size.height);
        _aboutViewController.owner = self;
    }
    return _aboutViewController;
}

- (GospelViewController *) gospelViewController{
    if (!_gospelViewController) {
        _gospelViewController = [[UIStoryboard storyboardWithName:@"Gospel" bundle:nil] instantiateViewControllerWithIdentifier:@"Gospel"];
    }
    return _gospelViewController;
}

- (HeaderView *) headerView{
    if (!_headerView) {
        _headerView = [[HeaderView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - HEADER_GAP_HEIGHT, self.view.frame.size.width, HEADER_GAP_HEIGHT)];
        //_headerView.hidden = YES;
        //_headerView.owner = self;
    }
    return _headerView;
}

#pragma mark view circle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self startAnimation];
    //[self.view setBackgroundColor:[UIColor colorWithHexString:@"e8e7ed"]];
    
    [self.view addSubview:self.btnDirectory];
    [self.view addSubview:self.btnAbout];
    [self.view addSubview:self.btnGospel];
    
    [self.view addSubview:self.btnShare];
    
    [self.view addSubview:self.imgVline1];
    [self.view addSubview:self.imgVline2];
    [self.view addSubview:self.containerView];
    [self.view addSubview:self.gosContainerView];
    [self.view addSubview:self.aboutContainerView];
    //[GestureUtility setDragForViewY:self.containerView withBeyondView:self.headerView];
}

- (void)viewWillAppear:(BOOL)animated{
   
}

- (void)viewDidAppear:(BOOL)animated{
    
    [self performSelector:@selector(setUpAnimation) withObject:nil afterDelay:0.5];
}

- (void)onShare:(UIButton *)sender{
    //NSLog(@"detail on share!!");
    [self showMailCompose];
}

#pragma self view animation
- (void) setUpAnimation{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.fromValue = [NSValue valueWithCGRect:CGRectMake(55, self.view.frame.origin.y - 168, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(55, 168, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    
    [self.btnDirectory pop_addAnimation:popOutAnimation forKey:@"DirectorySlideIn"];
    
    POPSpringAnimation *popOutAnimation2 = [POPSpringAnimation animation];
    popOutAnimation2.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation2.fromValue = [NSValue valueWithCGRect:CGRectMake(self.view.frame.origin.x+self.view.frame.size.width+MENU_BTN_WIDTH, 345, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
    popOutAnimation2.toValue = [NSValue valueWithCGRect:CGRectMake(55, 345, MENU_BTN_WIDTH, MENU_BTN_HEIGHT)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation2.springBounciness = 0;
    popOutAnimation2.springSpeed = 0.5;
    [self.btnAbout pop_addAnimation:popOutAnimation2 forKey:@"AboutSlideIn"];
    
    POPSpringAnimation *popOutAnimation3 = [POPSpringAnimation animation];
    popOutAnimation3.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation3.fromValue = [NSValue valueWithCGRect:CGRectMake(self.view.frame.origin.x - MENU_BTN_WIDTH, 232, MENU_BTN_WIDTH, 90)];
    popOutAnimation3.toValue = [NSValue valueWithCGRect:CGRectMake(55, 232, MENU_BTN_WIDTH, 90)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation3.springBounciness = 0;
    popOutAnimation3.springSpeed = 0.5;
    [self.btnGospel pop_addAnimation:popOutAnimation3 forKey:@"GospelSlideIn"];
    
    POPSpringAnimation *popOutTheLineAnimation = [POPSpringAnimation animation];
    popOutTheLineAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutTheLineAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(60,225, 194, 2)];
    popOutTheLineAnimation.velocity = [NSValue valueWithCGRect:CGRectMake(40, 0, 2, 40)];
    popOutTheLineAnimation.springBounciness = 10.0;
    popOutTheLineAnimation.springSpeed = 10.0;
    [self.imgVline1 pop_addAnimation:popOutTheLineAnimation forKey:@"drawLine1"];
    
    POPSpringAnimation *popOutTheLine2Animation = [POPSpringAnimation animation];
    popOutTheLine2Animation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutTheLine2Animation.toValue = [NSValue valueWithCGRect:CGRectMake(60,325, 194, 2)];
    popOutTheLine2Animation.velocity = [NSValue valueWithCGRect:CGRectMake(40, 0, 0, 40)];
    popOutTheLine2Animation.springBounciness = 10.0;
    popOutTheLine2Animation.springSpeed = 10.0;
    [self.imgVline2 pop_addAnimation:popOutTheLine2Animation forKey:@"drawLine2"];
}

- (void) onGospel:(UIButton *)btn{
    NSLog(@"onGospel");
    [self willMoveToParentViewController:nil];
    self.gosContainerView.hidden = FALSE;
    isContainerPresent = YES;
    [self addChildViewController:self.gospelViewController];
    self.gospelViewController.owner = self;
    
    [self.gosContainerView addSubview:self.gospelViewController.view];
    
    [self.view bringSubviewToFront:self.gosContainerView];
    [self willMoveToParentViewController:nil];
    [self animateOnGospelViewIn];
    //[GestureUtility setDragForViewY:self.gosContainerView withBeyondView:nil];
}

- (void) onAbout:(UIButton *)btn{
    NSLog(@"onDirectory");
    [self willMoveToParentViewController:nil];
    self.aboutContainerView.hidden = FALSE;
    isContainerPresent = YES;
    [self addChildViewController:self.aboutViewController];
    //self.aboutViewController.headerView = self.headerView;
    [self.aboutContainerView addSubview:self.aboutViewController.view];
    [self.view bringSubviewToFront:self.containerView];
    [self willMoveToParentViewController:nil];
    [self animateOnAboutViewIn];
    [self.aboutViewController reLoadTheView];
}

- (void) onDirectory:(UIButton *)btn{
    //NSLog(@"onDirectory");
    [self willMoveToParentViewController:nil];
    self.containerView.hidden = FALSE;
    isContainerPresent = YES;
    [self addChildViewController:self.navDirectoryViewController];
    
    [self.containerView addSubview:self.navDirectoryViewController.view];
    self.directoryViewController.headerView = self.headerView;
    [self.view bringSubviewToFront:self.containerView];
    [self willMoveToParentViewController:nil];
    [self animateOnDirectoryViewIn];
    [self.directoryViewController reLoadTheView];
}

- (void)registerEffectForView:(UIView *)aView depth:(CGFloat)depth
{
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        UIInterpolatingMotionEffect *effectX;
        UIInterpolatingMotionEffect *effectY;
        effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        
        
        effectX.maximumRelativeValue = @(depth);
        effectX.minimumRelativeValue = @(-depth);
        effectY.maximumRelativeValue = @(depth);
        effectY.minimumRelativeValue = @(-depth);
        
        [aView addMotionEffect:effectX];
        [aView addMotionEffect:effectY];
    }
}

#pragma mark header view animation
- (void) animateOnDirectoryViewIn{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.directoryViewController.view.frame.origin.x, HEADER_GAP_HEIGHT, self.directoryViewController.view.frame.size.width, self.directoryViewController.view.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    
    [self.containerView pop_addAnimation:popOutAnimation forKey:@"DirectoryViewSlideIn"];
    
    [self.view addSubview:self.headerView];
    //self.directoryViewController.headerView = self.headerView;
    self.headerView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, HEADER_GAP_HEIGHT);
    [self.headerView nameViewStyleSetup:[UIFont fontWithName:BLANCE_CONDENSED size:35] andTextColor:[UIColor whiteColor]];
    [self.headerView setNameText:@"DIRECTORY"];
    [self.headerView setTag:1];
    [self.headerView showFadeInView];
}

- (void) animateOnDirectoryViewOut{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.directoryViewController.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.directoryViewController.view.frame.size.width, self.directoryViewController.view.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    
    [self.containerView pop_addAnimation:popOutAnimation forKey:@"DirectoryViewSlideOut"];
    
    POPSpringAnimation *popOutHeaderAnimation = [POPSpringAnimation animation];
    popOutHeaderAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutHeaderAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-HEADER_GAP_HEIGHT, self.view.frame.size.width, HEADER_GAP_HEIGHT)];
    //popOutAnimation.velocity = @(1000);
    popOutHeaderAnimation.springBounciness = 0;
    popOutHeaderAnimation.springSpeed = 0.5;
    [self.headerView pop_addAnimation:popOutHeaderAnimation forKey:@"DirectoryViewSlideOut"];
}

- (void) animateOnAboutViewIn{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.aboutViewController.view.frame.origin.x, self.aboutViewController.view.frame.origin.y, self.aboutContainerView.frame.size.width, self.aboutContainerView.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    
    [self.aboutContainerView pop_addAnimation:popOutAnimation forKey:@"DirectoryViewSlideIn"];
    /*[self.view addSubview:self.headerView];
    self.headerView.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, HEADER_GAP_HEIGHT);
    [self.headerView nameViewStyleSetup:[UIFont fontWithName:BLANCE_CONDENSED size:35] andTextColor:[UIColor whiteColor]];
    [self.headerView setNameText:@"ABOUT"];
    [self.headerView setTag:2];
    [self.headerView showFadeInView];*/
}

- (void) animateOnAboutViewOut{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.aboutViewController.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.aboutViewController.view.frame.size.width, self.aboutViewController.view.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    [self.aboutContainerView pop_addAnimation:popOutAnimation forKey:@"AboutViewSlideOut"];
    
    /*POPSpringAnimation *popOutHeaderAnimation = [POPSpringAnimation animation];
    popOutHeaderAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutHeaderAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.view.frame.origin.x, 0-HEADER_GAP_HEIGHT, self.view.frame.size.width, HEADER_GAP_HEIGHT)];
    //popOutAnimation.velocity = @(1000);
    popOutHeaderAnimation.springBounciness = 0;
    popOutHeaderAnimation.springSpeed = 0.5;
    [self.headerView pop_addAnimation:popOutHeaderAnimation forKey:@"AboutViewSlideOut"];*/
}

- (void) animateOnGospelViewIn{
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.gospelViewController.view.frame.origin.x, self.gospelViewController.view.frame.origin.y, self.gospelViewController.view.frame.size.width, self.gospelViewController.view.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    [self.gosContainerView pop_addAnimation:popOutAnimation forKey:@"GospelViewSlideIn"];
    [self.gospelViewController reloadTheView];
}

- (void) animateOnGospelViewOut{
    NSLog(@"gospelViewController close!!");
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.gospelViewController.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.size.height, self.gospelViewController.view.frame.size.width, self.gospelViewController.view.frame.size.height)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    [self.gospelViewController unloadTheView];
    [self.gosContainerView pop_addAnimation:popOutAnimation forKey:@"GospelViewSlideOut"];
}

#pragma mark HeaderDelegateMethod
- (void) onHeaderTapToClose:(HeaderView *)headerView{
    if (headerView.tag == 1) {
        [self animateOnDirectoryViewOut];
        self.headerView = nil;
        //[self.directoryViewController.view removeFromSuperview];
    }
    else if(headerView.tag == 2){
        [self animateOnAboutViewOut];
        self.headerView = nil;
        //[self.aboutViewController.view removeFromSuperview];
    }
}

#pragma mark directoryViewControllerDelegate
- (void) onHeaderViewTap:(DirectoryViewController *)viewController andHeaderView:(HeaderView *)headerView{
    if (headerView.tag == 1) {
        [self animateOnDirectoryViewOut];
        self.headerView = nil;
        [self.navDirectoryViewController.view removeFromSuperview];
    }
    else if(headerView.tag == 2){
        [self animateOnAboutViewOut];
        self.headerView = nil;
        [self.aboutViewController.view removeFromSuperview];
    }
}

#pragma mark gospelViewDelegate
- (void) onViewClose:(GospelViewController *)viewController{
    [self animateOnGospelViewOut];
}

- (void) onAboutViewClose:(AboutViewController *)viewController{
    [self animateOnAboutViewOut];
}

@end
