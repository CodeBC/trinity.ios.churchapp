//
//  Message.h
//  Church
//
//  Created by Zayar on 6/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, retain) NSNumber * animation_type;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSString * detail_image;
@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSString * tittle;
@property (nonatomic, retain) NSNumber * order_id;

@end
