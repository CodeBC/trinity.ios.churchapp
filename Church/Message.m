//
//  Message.m
//  Church
//
//  Created by Zayar on 6/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic animation_type;
@dynamic detail;
@dynamic detail_image;
@dynamic idx;
@dynamic image_url;
@dynamic tittle;
@dynamic order_id;

@end
