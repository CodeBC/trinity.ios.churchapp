//
//  MessageDetailView.h
//  Church
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
@protocol MessageDetailViewDelegate;
@interface MessageDetailView : UIView
@property id<MessageDetailViewDelegate> owner;
- (void)setUpViews;
- (void) loadTheViewWithMessage:(Message *)obj;
@end

@protocol MessageDetailViewDelegate
- (void) onShareFromMessageDetail:(MessageDetailView *)msgDetailView;
@end
