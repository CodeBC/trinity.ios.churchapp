//
//  MessageDetailView.m
//  Church
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MessageDetailView.h"
@interface MessageDetailView()
@property (nonatomic,strong) UIWebView * webView;
@property (nonatomic,strong) UIButton * btnShare;
@end
@implementation MessageDetailView
@synthesize owner;
- (UIWebView *) webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    return _webView;
}

- (UIButton *) btnShare{
    if (!_btnShare) {
        _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnShare setFrame:CGRectMake(290, self.frame.size.height-40, 21, 28)];
        [_btnShare setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
        [_btnShare addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnShare;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setUpViews{
    [self addSubview:self.webView];
    [self addSubview:self.btnShare];
}

- (void) loadTheViewWithMessage:(Message *)obj{
    [self.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"AvenirNext-Regular\" size=\"2\" color='#000'> <br/> %@ </font></body></html>",obj.detail] baseURL:nil];
}

- (void)onShare:(UIButton *)sender{
    //NSLog(@"detail on share!!");
    [owner onShareFromMessageDetail:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
