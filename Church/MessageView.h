//
//  MessageView.h
//  Church
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
@protocol MessageViewDelegate;
@interface MessageView : UIView
@property (nonatomic) id<MessageViewDelegate> owner;
- (void)loadTheViewWithMessage:(Message *)obj;
- (void)setUpViews;
- (void)removeMainImage;
- (void)setMainImageWithAnimation;
- (void)hideDarkOverlay;
- (void)unloadTheScroll;
@end
@protocol MessageViewDelegate
- (void)messageScroll:(UIScrollView *)msgScrollView andView:(MessageView *)msgView;
- (void)onShareWithMessage:(Message *)objMsg andView:(MessageView *)msgView;

@end
