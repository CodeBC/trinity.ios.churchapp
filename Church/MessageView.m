//
//  MessageView.m
//  Church
//
//  Created by Zayar on 5/9/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MessageView.h"
#import "StringTable.h"
#import "MessageDetailView.h"
#import "UIImageView+WebCache.h"
#import "GestureUtility.h"
#import "SDWebImageManager.h"
#import "DACircularProgressView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "Message.h"
#import "CircleView.h"
#import "Utility.h"

@interface MessageView()
{
    NSInteger currentPagingIndex1;
    Message * currentMessage;
    BOOL isAnimationShouldStop;
}
@property (nonatomic,strong) UIScrollView * messageScrollView;
@property (nonatomic,strong) UIImageView * imgBgView;
@property (nonatomic,strong) UIImageView * imgMessageView;
@property (nonatomic,strong) UIImageView * imgMainMessageView;
@property (nonatomic,strong) UIView * darkOverlayView;
@property (nonatomic,strong) UIButton * btnDetail;
@property (nonatomic,strong) MessageDetailView * msgDetailView;
@property (assign, nonatomic) CGFloat currentProgress;
@property (strong, nonatomic) DACircularProgressView *progressView;
@property (nonatomic, strong) NSMutableArray * arrCircleViews;
@end
@implementation MessageView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpViews];
    }
    return self;
}

- (NSMutableArray *) arrCircleViews{
    if (!_arrCircleViews) {
        _arrCircleViews = [[NSMutableArray alloc] init];
    }
    return _arrCircleViews;
}

- (void) loadTheCirclesView{
    if (![self.arrCircleViews count]) {
        int totalCircle = 5;
        for (int i = 1; i <= totalCircle; i++) {
            CGFloat x = (CGFloat) (arc4random() % (int) self.bounds.size.width);
            CGFloat y = (CGFloat) (arc4random() % (int) self.bounds.size.height);
            CircleView * circleView = [[CircleView alloc] initWithFrame:CGRectMake(x, y, 150, 150)];
            [circleView setAlpha:0.5];
            [self addSubview:circleView];
            [self.arrCircleViews addObject:circleView];
        }
    }
}


- (UIScrollView *)messageScrollView{
    if (!_messageScrollView) {
        _messageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _messageScrollView.pagingEnabled = YES;
        _messageScrollView.delegate = self;
    }
    return _messageScrollView;
}

/*- (MRCircularProgressView *) circularProgressView{
    if (!_circularProgressView) {
        _circularProgressView = [[MRCircularProgressView alloc] initWithFrame:CGRectMake(self.center.x-30, self.center.y-30, 60, 60)];
        _circularProgressView.hidden = TRUE;
        _circularProgressView.delegate = self;
        _circularProgressView.progressColor = [UIColor whiteColor];
        _circularProgressView.backgroundColor = [UIColor clearColor];
    }
    return _circularProgressView;
}*/

- (UIView *)darkOverlayView{
    if (!_darkOverlayView) {
        _darkOverlayView = [[UIView alloc] initWithFrame:CGRectMake(self.imgMainMessageView.frame.origin.x + self.imgMainMessageView.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
        _darkOverlayView.backgroundColor = [UIColor blackColor];
        _darkOverlayView.alpha = 0.4;
    }
    return _darkOverlayView;
}

- (DACircularProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[DACircularProgressView alloc] initWithFrame:CGRectMake(self.imgMainMessageView.center.x-30, self.imgMainMessageView.center.y-30, 60, 60)];
        _progressView.roundedCorners = YES;
        _progressView.trackTintColor = [UIColor clearColor];
    }
    return _progressView;
}

- (UIImageView *)imgMainMessageView{
    if (!_imgMainMessageView) {
        _imgMainMessageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _imgMainMessageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgMainMessageView;
}

- (UIImageView *)imgMessageView{
    if (!_imgMessageView) {
        _imgMessageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.imgMainMessageView.frame.origin.x + self.imgMainMessageView.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
        _imgMessageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgMessageView;
}

- (UIImageView *)imgBgView{
    if (!_imgBgView) {
        _imgBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        //_imgBgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgBgView;
}

- (UIButton *)btnDetail{
    if (!_btnDetail) {
        _btnDetail = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnDetail setFrame:CGRectMake(0, self.imgMainMessageView.frame.origin.y +self.imgMainMessageView.frame.size.height, self.frame.size.width, self.frame.size.height)];
        [_btnDetail setBackgroundColor:[UIColor clearColor]];
        [_btnDetail setTitle:@"" forState:UIControlStateNormal];
        [_btnDetail addTarget:self action:@selector(onDetailClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnDetail;
}

- (void)removeMainImage{
    [self.imgMainMessageView setImage:nil];
}

- (void)setMainImageWithAnimation{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [self.progressView setHidden:NO];
    [manager downloadWithURL:[NSURL URLWithString:currentMessage.image_url] options:SDWebImageContinueInBackground progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        CGFloat receivedFSize = (float) receivedSize;
        CGFloat expectedFSize = (float) expectedSize;
        //NSNumber *progress1 = [NSNumber numberWithFloat:(receivedSize/expectedSize)];
        CGFloat progress1 = ((receivedFSize/expectedFSize)*100)/100;
        NSLog(@"progress %f and received size %d and expected size %d and calculated %f",progress1,receivedSize,expectedSize,(receivedFSize/expectedFSize)*100);
        /*if (progress1>0) {
         
         }*/
        [self.progressView setHidden:NO];
        [self.progressView setProgress:progress1 animated:YES];
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
        [self.progressView setHidden:YES];
        UIViewAnimationOptions animationOption;
        switch ([currentMessage.animation_type integerValue]) {
            case 1:
                animationOption = UIViewAnimationOptionTransitionCrossDissolve;
                break;
            case 2:
                animationOption = UIViewAnimationOptionTransitionFlipFromBottom;
                break;
            case 3:
                animationOption = UIViewAnimationOptionTransitionFlipFromLeft;
                break;
            case 4:
                animationOption = UIViewAnimationOptionTransitionFlipFromRight;
                break;
            case 5:
                animationOption = UIViewAnimationOptionTransitionFlipFromTop;
                break;
                
            default:
                break;
        }
        [UIView transitionWithView:self.imgMainMessageView
                          duration:1
                           options:animationOption
                        animations:^{
                            [self.imgMainMessageView setImage:image];
                        } completion:NULL];
        [self bringSubviewToFront:self.imgMainMessageView];
    }];
}

- (MessageDetailView *)msgDetailView{
    if (!_msgDetailView) {
        _msgDetailView = [[MessageDetailView alloc] initWithFrame:CGRectMake(0, self.imgMessageView.frame.origin.y +self.imgMessageView.frame.size.height, self.frame.size.width, self.frame.size.height-HEADER_GAP_HEIGHT)];
        _msgDetailView.owner = self;
        [_msgDetailView setUpViews];
    }
    return _msgDetailView;
}

- (void) setUpViews{
    
    [self setBackgroundColor:[UIColor blackColor]];
    [self addSubview:self.imgBgView];
    [self addSubview:self.messageScrollView];
    [self.messageScrollView addSubview:self.imgMainMessageView];
    [self.messageScrollView addSubview:self.darkOverlayView];
    [self.messageScrollView addSubview:self.imgMessageView];
    [self.messageScrollView addSubview:self.btnDetail];
    [self.messageScrollView addSubview:self.msgDetailView];
    [self.imgMainMessageView addSubview:self.progressView];
    self.progressView.hidden = TRUE;
}

- (void) onDetailClick:(UIButton *)sender {
    //NSLog(@"%s", __FUNCTION__);
    NSLog(@"on detail!!!!");
    [self scrollViewScrollsToEnd];
}

- (void) loadTheViewWithMessage:(Message *)obj{
    currentMessage = obj;
    NSLog(@"img main link %@ detail link %@",obj.image_url,obj.detail_image);
    switch ([obj.animation_type integerValue]) {
        case 1:
            [self.imgBgView setImage:[UIImage imageNamed:@"message01_bg"]];
            break;
        case 2:
            [self.imgBgView setImage:[UIImage imageNamed:@"message02_bg"]];
            break;
        case 3:
            [self.imgBgView setImage:[UIImage imageNamed:@"message03_bg"]];
            break;
        case 4:
            [self.imgBgView setImage:[UIImage imageNamed:@"message04_bg"]];
            break;
        case 5:
            [self.imgBgView setImage:[UIImage imageNamed:@"message05_bg"]];
            break;
        default:
            break;
    }
    
    [self.imgMessageView setImageWithURL:[NSURL URLWithString:obj.detail_image] placeholderImage:nil options:SDWebImageContinueInBackground usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.imgMainMessageView setImage:nil];
    self.currentProgress = 0.0f;
    
    //[self.msgDetailView loadTheViewWithMessage:obj];
    [self performSelector:@selector(setMainImageWithAnimation) withObject:nil afterDelay:0.5];
    
    NSLog(@"content heght %f",self.msgDetailView.frame.origin.y+self.msgDetailView.frame.size.height);
    [self.messageScrollView setContentSize:CGSizeMake(self.imgMessageView.frame.origin.x + self.imgMessageView.frame.size.width,self.frame.size.height)];
    
    /*if ([obj.idx isEqualToString:@"7"]) {
        [self.darkOverlayView setAlpha:0];
    }*/
    
}

- (void) hideDarkOverlay{
    [self.darkOverlayView setAlpha:0];
    [self.darkOverlayView setHidden:YES];
    self.darkOverlayView = nil;
    NSLog(@"hidding overlay!!!");
}

- (void) scrollViewScrollsToEnd{
    CGPoint offset = CGPointMake(0, self.messageScrollView.contentSize.height - self.messageScrollView.frame.size.height);
    [self.messageScrollView setContentOffset:offset animated:YES];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    //NSLog(@"scroll view x %f",scrollView.contentOffset.x);
    [owner messageScroll:scrollView andView:self];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if( page != currentPagingIndex1 ){
        currentPagingIndex1 = page;
        if (currentPagingIndex1 != 0) {
            [self.imgMainMessageView setImage:nil];
        }
        else if(currentPagingIndex1 == 0 ){
            [self setMainImageWithAnimation];
        }
    }
    NSLog(@"current page index %d",currentPagingIndex1);
}

- (void) onShareFromMessageDetail:(MessageDetailView *)msgDetailView{
    [owner onShareWithMessage:currentMessage andView:self];
}

#pragma mark circle views
- (void) startAnimation{
    [self animationLoop:@"" finished:0 context:nil];
}

- (void)registerEffectForView:(UIView *)aView depth:(CGFloat)depth
{
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        UIInterpolatingMotionEffect *effectX;
        UIInterpolatingMotionEffect *effectY;
        effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        
        
        effectX.maximumRelativeValue = @(depth);
        effectX.minimumRelativeValue = @(-depth);
        effectY.maximumRelativeValue = @(depth);
        effectY.minimumRelativeValue = @(-depth);
        
        [aView addMotionEffect:effectX];
        [aView addMotionEffect:effectY];
    }
}

- (void)stopAnimation{
    for (CircleView * cView in self.arrCircleViews) {
        isAnimationShouldStop = TRUE;
        [cView.layer removeAllAnimations];
        [cView removeFromSuperview];
    }
}

-(void)animationLoop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (!isAnimationShouldStop) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:20];
        // remove:
        //  [UIView setAnimationRepeatCount:1000];
        //  [UIView setAnimationRepeatAutoreverses:YES];
        for (CircleView * cView in self.arrCircleViews) {
            CGFloat x = (CGFloat) (arc4random() % (int) self.bounds.size.width);
            CGFloat y = (CGFloat) (arc4random() % (int) self.bounds.size.height);
            
            CGFloat a1 = (CGFloat) (arc4random() % (int) 1.5);
            CGPoint squarePostion = CGPointMake(x, y);
            cView.center = squarePostion;
            [cView setAlpha:a1];
        }
        // add:
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationLoop:finished:context:)];
        
        [UIView commitAnimations];
    }
}

- (void)unloadTheScroll{
    NSLog(@"MessageView unloadTheScroll");
    [self.imgMainMessageView setImage:nil];
    currentPagingIndex1 = 0;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
