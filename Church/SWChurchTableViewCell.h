//
//  SWTableViewCell.h
//  SWTableViewCell
//
//  Created by Chris Wendel on 9/10/13.
//  Copyright (c) 2013 Chris Wendel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Church.h"
@class SWChurchTableViewCell;

@protocol SWTableViewCellDelegate <NSObject>

- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index;
- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
- (void)didSelectedTheCell:(SWChurchTableViewCell *)cell;

@end

@interface SWChurchTableViewCell : UITableViewCell
{
    /**** for Schedule cell ****/
    
    /*IBOutlet UILabel * lblVerticalStrip;
    IBOutlet UILabel * lblTitle;
    IBOutlet UILabel * lblSpeaker;
    IBOutlet UIButton * btnLocation;
    IBOutlet UILabel * lblTime;*/
    UIImageView * imgView;
    UILabel * lblName;
    UILabel * lblAuthorName;
    UILabel * lblHeadingWord;
    UIView * accessoryLine;
    /**** end for Schedule cell ****/
}

@property (nonatomic, strong) NSArray *leftUtilityButtons;
@property (nonatomic, strong) NSArray *rightUtilityButtons;
@property (nonatomic) id <SWTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *mainView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier height:(CGFloat)height leftUtilityButtons:(NSArray *)leftUtilityButtons rightUtilityButtons:(NSArray *)rightUtilityButtons;

- (void)wayToOriginalScrollView;

/**** for Schedule cell ****/
@property (nonatomic, strong) Church * church;
- (void)loadTheViewWith:(Church *)obj;
- (void) reloadTheScrollViewHeight:(float)cellHeight;
- (void) loadTheView:(Church *) obj;
- (void) loadTheViewForAuthorPoem:(Church *) obj;
/**** end for Schedule cell ****/
@end

@interface NSMutableArray (SWUtilityButtons)

- (void)addUtilityButtonWithColor:(UIColor *)color title:(NSString *)title;
- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon;
- (void)addUtilityButtonWithColor:(UIColor *)color icon:(UIImage *)icon andTag:(int)tag;



@end
