//
//  SearchByLocationViewController.h
//  Church
//
//  Created by Zayar on 5/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Church.h"
@protocol SearchByLocationViewControllerDelegate;
@interface SearchByLocationViewController : UIViewController
@property (nonatomic) id<SearchByLocationViewControllerDelegate> owner;
@end

@protocol SearchByLocationViewControllerDelegate
- (void) goToDirectoryDetailFromSearchByLocation:(SearchByLocationViewController *)searchByLocationViewController andChurch:(Church *)obj;
@end
