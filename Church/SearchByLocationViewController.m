//
//  SearchByLocationViewController.m
//  Church
//
//  Created by Zayar on 5/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SearchByLocationViewController.h"
#import "ChurchAPIClient.h"
#import "StringTable.h"
#import "Utility.h"
#import "Church.h"
#import "ServiceTime.h"
#import "SWChurchTableViewCell.h"
#import <MessageUI/MessageUI.h>
#import "DirectoryDetailViewController.h"
@interface SearchByLocationViewController ()
{
    CGFloat currentLocationLat;
    CGFloat currentLocationLong;
   
}
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) MFMailComposeViewController * mailComposeViewController;
@property (nonatomic,strong) DirectoryDetailViewController * detailViewController;
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) NSMutableArray * arrChurch;// of church

@end

@implementation SearchByLocationViewController
@synthesize owner;
- (MFMailComposeViewController *) mailComposeViewController{
    if (!_mailComposeViewController) {
        _mailComposeViewController = [[MFMailComposeViewController alloc] init];
        _mailComposeViewController.mailComposeDelegate = self;
    }
    return _mailComposeViewController;
}

- (NSMutableArray *)arrChurch{
    if (!_arrChurch) {
        _arrChurch = [[NSMutableArray alloc] init];
    }
    return _arrChurch;
}

- (CLLocationManager *) locationManager{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return _locationManager;
}

- (DirectoryDetailViewController *) detailViewController{
    if (!_detailViewController) {
        _detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectoryDetail"];
    }
    return _detailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentLocationLat = 0;
    [self.locationManager startUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void) reLoadTheView{
    [self.tbl reloadData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    /*UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];*/
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@ :lat: %.6f lon: %.6f", newLocation,newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation.coordinate.longitude != 0 && currentLocationLat == 0) {
        
        currentLocationLat = currentLocation.coordinate.latitude;
        currentLocationLong = currentLocation.coordinate.longitude;
        
        //[self syncLocation:1.3268938 andlon:103.7247749];
        [self syncLocation:currentLocationLat andlon:currentLocationLong];
    }
}

- (void)syncLocation:(CGFloat)lat andlon:(CGFloat)lon{
    [SVProgressHUD show];
    NSDictionary * param = @{@"latitude":[NSString stringWithFormat:@"%.6f",lat],@"longitude":[NSString stringWithFormat:@"%.6f",lon]};
    [[ChurchAPIClient sharedClient] POST:[NSString stringWithFormat:@"%@",NEAR_BY_LINK] parameters:param success:^(AFHTTPRequestOperation *operation, id json) {
        
        NSLog(@"syncChurch successfully return!!! %@",json);
        NSDictionary * myDict = (NSDictionary *)json;
        
        NSArray * arr = (NSArray *)myDict;
        //NSLog(@"syncChurch count %d",[self.arrChurch count]);
        if ([arr count]) {
            [self.arrChurch removeAllObjects];
        }
        for(NSInteger i=0;i<[arr count];i++){
            NSDictionary * dicEmp = (NSDictionary *)[arr objectAtIndex:i];
            NSInteger idx = [[dicEmp objectForKey:@"id"] integerValue];
            NSString * strIdx = [NSString stringWithFormat:@"%d",idx];
            NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx == %@",strIdx];
            Church * church = [Church MR_findFirstWithPredicate:predicate];
            if ([dicEmp objectForKey:@"distance"] != [NSNull null]) {
                CGFloat distance = [[dicEmp objectForKey:@"distance"] floatValue];
                church.distance = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",distance]];
            }
            [self.arrChurch addObject:church];
        }
        self.arrChurch = (NSMutableArray *)[self.arrChurch sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            CGFloat first = [[(Church*)a distance] floatValue];
             CGFloat second = [[(Church*)b distance] floatValue];
            if (first > second) {
                return (NSComparisonResult)NSOrderedDescending;
            } else if (first < second) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        [self reLoadTheView];
        [SVProgressHUD dismiss];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
        [SVProgressHUD dismiss];
    }];

}

- (void) insertChurch:(Church *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    [obj MR_inContext:localContext];
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Church saved");
    }];
}

- (void) updateChurch:(Church *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idx ==[c] %@ ", obj.idx];
    Church * churchFound = [Church MR_findFirstWithPredicate:predicate inContext:localContext];
    if (churchFound) {
        churchFound = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            //Church * objChurch = [Church MR_findFirstByAttribute:@"idx" withValue:obj.server_id inContext:localContext];
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    SWChurchTableViewCell *cell = (SWChurchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSLog(@"UI table width %f",cell.frame.size.width);
    if (cell == nil) {
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        //[rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"8EC447"] icon:[UIImage imageNamed:@"Star_Unselect2"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"phone_icon"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"location-icon"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"envelope_icon"] andTag:indexPath.row];
        
        cell = [[SWChurchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier height:71 leftUtilityButtons:leftUtilityButtons rightUtilityButtons:rightUtilityButtons];
        
    }
    cell.delegate = self;
    cell.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    Church * obj = [self.arrChurch objectAtIndex:indexPath.row];
    //NSLog(@"home author id %@",obj.author_id);
    /* UIButton *button = [cell.rightUtilityButtons objectAtIndex:0];
     if ([obj.is_fav intValue]==1) {
     [button setImage:[UIImage imageNamed:@"748-heart-filled"] forState:UIControlStateNormal];
     }
     else if([obj.is_fav intValue]==1){
     [button setImage:[UIImage imageNamed:@"748-heart"] forState:UIControlStateNormal];
     }*/
    
    [cell loadTheView:obj];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (homeDetailViewController == nil) {
     homeDetailViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"detailViewController"];
     }
     Poems * obj = [arrPoems objectAtIndex:indexPath.row];
     homeDetailViewController.objPoem = obj;
     [self.navigationController pushViewController:homeDetailViewController animated:YES];
     //[self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrChurch count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark - SWTableViewDelegate
- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    Church * obj = [self.arrChurch objectAtIndex:cell.tag];
    switch (index) {
        case 0:
        {
            /*NSLog(@"More button was pressed and cell tag %d",cell.tag);
             Poems * obj = [self.arrPoems objectAtIndex:cell.tag];
             UIButton *button = [cell.rightUtilityButtons objectAtIndex:index];
             NSLog(@"obj fav %d",[obj.is_fav intValue]);
             //button.backgroundColor = color;
             AppDelegate * delegate = [[UIApplication sharedApplication]delegate];
             NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
             if ([obj.is_fav intValue] == 0) {
             [button setImage:[UIImage imageNamed:@"748-heart-filled"] forState:UIControlStateNormal];
             obj.is_fav = [NSNumber numberWithInt:1];
             [self.arrPoems replaceObjectAtIndex:cell.tag withObject:obj];
             [cell wayToOriginalScrollView];
             }
             else if ([obj.is_fav intValue] == 1){
             [button setImage:[UIImage imageNamed:@"748-heart"] forState:UIControlStateNormal];
             obj.is_fav = [NSNumber numberWithInt:0];
             [self.arrPoems replaceObjectAtIndex:cell.tag withObject:obj];
             [cell wayToOriginalScrollView];
             }
             [delegate updatePoem:obj withLocalContext:localContext];*/
            [self onCallWithChurch:obj];
            break;
        }
            
        case 1:
        {
            [self onLocationWithChurch:obj];
            break;
        }
        case 2:
        {
            [self showMailComposeWithChurch:obj];
            break;
        }
        default:
            break;
    }
}

- (void) onCallWithChurch:(Church *)obj{
    NSString * number = [NSString stringWithFormat:@"tel://%@", [obj.telephone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    NSLog(@"Calling %@", number);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: number]];
}

- (void) showMailComposeWithChurch:(Church *)obj{
    // Email Subject
    NSString *emailTitle = @"Church";
    // Email Content
    NSString *messageBody = @"Church mail!";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:obj.email];
    
    
    [self.mailComposeViewController setSubject:emailTitle];
    [self.mailComposeViewController setMessageBody:messageBody isHTML:NO];
    [self.mailComposeViewController setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onLocationWithChurch:(Church *)obj{
    NSString* addr = nil;
    addr = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f&saddr=Current", [obj.lat floatValue],[obj.lon floatValue]];
    NSURL* url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didSelectedTheCell:(SWChurchTableViewCell *)cell{
    NSLog(@"location selected!!");
    Church * obj = [self.arrChurch objectAtIndex:cell.tag];
    [owner goToDirectoryDetailFromSearchByLocation:self andChurch:obj];
    //[self onSelectDay:nil];
}


@end
