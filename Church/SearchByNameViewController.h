//
//  SearchByNameViewController.h
//  Church
//
//  Created by Zayar on 5/20/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
#import "Church.h"
@protocol SearchByNameViewControllerDelegate;
@interface SearchByNameViewController : UITableViewController
@property (nonatomic) id<SearchByNameViewControllerDelegate> owner;
- (void) endSearchBarEditing;
- (void) reLoadTheView;
@end
@protocol SearchByNameViewControllerDelegate
- (void) goToDirectoryDetailFromSearchByName:(SearchByNameViewController *)searchByNameViewController andChurch:(Church *)obj;
@end
