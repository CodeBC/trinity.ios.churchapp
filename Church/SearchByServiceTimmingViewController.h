//
//  SearchByServiceTimmingViewController.h
//  Church
//
//  Created by Zayar on 5/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Church.h"
@protocol SearchByServiceTimmingViewControllerDelegate;
@interface SearchByServiceTimmingViewController : UIViewController
@property (nonatomic) id<SearchByServiceTimmingViewControllerDelegate> owner;
- (void) popTheViewController;
@end
@protocol SearchByServiceTimmingViewControllerDelegate
- (void) goToDirectoryDetailFromSearchByServiceTimming:(SearchByServiceTimmingViewController *)searchByNameViewController andChurch:(Church *)obj;

@end
