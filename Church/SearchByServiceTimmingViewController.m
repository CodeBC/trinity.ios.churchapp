//
//  SearchByServiceTimmingViewController.m
//  Church
//
//  Created by Zayar on 5/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SearchByServiceTimmingViewController.h"
#import "Utility.h"
#import "StringTable.h"
#import "ServiceTime.h"
#import "SearchResultViewController.h"
#import "Church.h"
@interface SearchByServiceTimmingViewController ()
{
    NSArray * arrDays;
    NSArray * arrTime;
    NSArray * arrDayNight;
    NSInteger selectedDayIndex;
    NSInteger selectedTimeIndex;
    NSInteger selectedDayNightIndex;
}
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectDay;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectTime;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) UIActionSheet *menu;
@property (strong, nonatomic) SearchResultViewController * searchResultViewController;
@end

@implementation SearchByServiceTimmingViewController
@synthesize owner;
- (UIActionSheet *)menu{
    if (!_menu) {
        _menu = [[UIActionSheet alloc] initWithTitle:@"Select Gender"
                                            delegate:self
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil];
    }
    return _menu;
}

- (SearchResultViewController *) searchResultViewController{
    if (!_searchResultViewController) {
        _searchResultViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchResult"];
        _searchResultViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    return _searchResultViewController;
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"service timming height %f",self.view.frame.size.height);
}

- (void) popTheViewController{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIPickerView *)pickerView{
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,320,260)];
        _pickerView.delegate = self;
        _pickerView.showsSelectionIndicator = YES;
    }
    return _pickerView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utility makeBorder:self.btnSelectDay andWidth:1 andColor:[UIColor colorWithHexString:@"4099a1"]];
    [Utility makeBorder:self.btnSelectTime andWidth:1 andColor:[UIColor colorWithHexString:@"4099a1"]];

    [Utility makeCornerRadius:self.btnSearch andRadius:3];
    [Utility makeCornerRadius:self.btnSelectDay andRadius:3];
    [Utility makeCornerRadius:self.btnSelectTime andRadius:3];

    CGRect toolbarFrame = CGRectMake(0, 0, self.view.bounds.size.width, 44);
    UIToolbar* controlToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];

    [controlToolbar setBarStyle:UIBarStyleBlack];
    [controlToolbar sizeToFit];

    UIBarButtonItem* spacer =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                  target:nil
                                                  action:nil];
    UIBarButtonItem* cancelButton;
    UIBarButtonItem* setButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(dismissAndSelectActivityActionSheet)];
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIBarButtonItemStyleDone
                                                   target:self
                                                   action:@selector(dismissAndCancelActivityActionSheet)];
    [controlToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer, setButton, nil]
                    animated:NO];
    [self.menu addSubview:controlToolbar];
    [self hardCodeDays];
    [self hardCodeTime];
    //selectedDayIndex = -1;
    //selectedTimeIndex = -1;
}

- (void) hardCodeDays{
    arrDays = [NSArray arrayWithObjects:@"Any day",@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",nil];
}

- (void) hardCodeTime{
    arrTime = [NSArray arrayWithObjects:@"Anytime",@"1:00-1:59",@"2:00-2:59",@"3:00-3:59",@"4:00-4:59",@"5:00-5:59",@"6:00-6:59",@"7:00-7:59",@"8:00-8:59",@"9:00-9:59",@"10:00-10:59",@"11:00-11:59",@"12:00-12:59",nil];
    arrDayNight = [NSArray arrayWithObjects:@"AM",@"PM",nil];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if (pickerView.tag == 0) {
        NSString *strName = [arrDays objectAtIndex:row];
        //ObjectCity * objCity = [arrCity objectAtIndex:row];
        //NSLog(@"city 1 name %@",objCity.strName);

        return strName;

    }
    if (pickerView.tag == 1) {

        if (component == 0) {
            NSString *strName = [arrTime objectAtIndex:row];
            //ObjectCity * objCity = [arrCity objectAtIndex:row];
            //NSLog(@"city 1 name %@",objCity.strName);

            return strName;
        }
        else if(component == 1){
            NSString *strName = [arrDayNight objectAtIndex:row];
            //ObjectCity * objCity = [arrCity objectAtIndex:row];
            //NSLog(@"city 1 name %@",objCity.strName);
            return strName;
        }

    }
    return @"";
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (pickerView.tag == 0) {
        return 1;
    }
    else if (pickerView.tag == 1) {
        return 2;
    }
    return 0;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 0) {
        return [arrDays count];
    }
    if (pickerView.tag == 1) {
        if (component == 0) {
            return [arrTime count];
        }
        else if(component == 1){
            return [arrDayNight count];
        }

    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow>>>>didSelectRow");
    if (pickerView.tag == 0) {
        selectedDayIndex = row;
    }
    if (pickerView.tag == 1) {
        if (component == 0) {
            selectedTimeIndex = row;
        }
        else if(component == 1){
            selectedDayNightIndex = row;
        }
    }

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //zBoxAppDelegate * delegate = [[UIApplication sharedApplication] delegate];

    if (buttonIndex == 0) {
        //self.label.text = @"Destructive Button";
        NSLog(@"Cancel Button");
    }

    else if (buttonIndex == 1) {
        //NSLog(@"Other Button Done Clicked and selected index %d",self.selectedLevel);

        if (self.pickerView.tag == 0){
            NSString * strName = [arrDays objectAtIndex:selectedDayIndex];
            [self.btnSelectDay setTitle:strName forState:normal];
            [self.menu dismissWithClickedButtonIndex:1 animated:YES];
        }
        if (self.pickerView.tag == 1){
            NSString * strName = [arrTime objectAtIndex:selectedTimeIndex];
            NSString * strName1 = [arrDayNight objectAtIndex:selectedDayNightIndex];
            if (selectedTimeIndex == 0) {
                strName1 = @"";
            }
            [self setBtnSelectTimeTitle:strName andDayNight:strName1];
            [self.menu dismissWithClickedButtonIndex:1 animated:YES];
        }
    }
}

- (void) setBtnSelectTimeTitle:(NSString *)strTime andDayNight:(NSString *)strDay{
    NSString * strTitle = [NSString stringWithFormat:@"%@ %@",strTime,strDay];
    [self.btnSelectTime setTitle:strTitle forState:UIControlStateNormal];
}

- (IBAction)onSelectDay:(UIButton *)sender {
    [self.menu setTitle:@"Select Day"];
    self.pickerView.tag = 0;
    self.pickerView.delegate = self;
    [self.pickerView reloadAllComponents];
    [self.pickerView selectRow:selectedDayIndex inComponent:0 animated:YES];
    NSLog(@"UIPicker width %f",self.pickerView.frame.size.width);
    [self.menu addSubview:self.pickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (IBAction)onSelectTime:(UIButton *)sender {
    [self.menu setTitle:@"Select Gender"];
    //UIButton * btn = (UIButton *)sender;
    // Add the picker
    self.pickerView.tag = 1;
    self.pickerView.delegate = self;
    [self.pickerView reloadAllComponents];
    [self.pickerView selectRow:selectedTimeIndex inComponent:0 animated:YES];
    [self.pickerView selectRow:selectedDayNightIndex inComponent:1 animated:YES];
    NSLog(@"UIPicker width %f",self.pickerView.frame.size.width);
    [self.menu addSubview:self.pickerView];
    [self.menu showInView:self.view];
    [self.menu setBounds:CGRectMake(0,0,320,ACTIONSHEET_HEIGHT)];
}

- (IBAction)onSearch:(UIButton *)sender {
    NSString * strName = [arrDays objectAtIndex:selectedDayIndex];

    NSString * strName1 = [arrDayNight objectAtIndex:selectedDayNightIndex];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"day contains[c] %@ AND (start_am_pm contains[c] %@ OR end_am_pm contains[c] %@)",strName,strName1,strName1];
    NSInteger selectedTimetick = [self getTimetickByHourIndex:selectedTimeIndex andDay:strName1];
    NSInteger selectedEndTimetick = [self getTimetickByHourIndex:selectedTimeIndex andDay:strName1] + 59;
     //NSLog(@"selectedTimetick :%i service selectedEndTimetick %i",selectedTimetick,selectedEndTimetick);
    NSLog(@"selected selectedTimetick %i", selectedTimetick);
    NSPredicate * predicate;
    if (selectedDayIndex == 0 && selectedTimeIndex == 0) {
        predicate = [NSPredicate predicateWithFormat:@"start_hour_timetick <> 0"];
    }
    else if (selectedDayIndex == 0) {
        predicate = [NSPredicate predicateWithFormat:@"start_hour_timetick BETWEEN {%d,%d}",selectedTimetick,selectedEndTimetick];
    }
    else if (selectedTimeIndex == 0){
        predicate = [NSPredicate predicateWithFormat:@"day contains[c] %@",strName];
    }
    else{
        predicate = [NSPredicate predicateWithFormat:@"day contains[c] %@ AND start_hour_timetick BETWEEN {%d,%d}",strName,selectedTimetick,selectedEndTimetick];
    }


    NSArray * arrServiceTime = [ServiceTime MR_findAllWithPredicate:predicate];

    NSMutableArray * arrOriginalArr = [[NSMutableArray alloc] init];
    for (ServiceTime * sTime in arrServiceTime) {
        //NSLog(@"church id %@ and stimetick %i",[NSString stringWithFormat:@"%i:%@%@",[sTime.start_hour integerValue],sTime.start_minute,sTime.start_am_pm],[sTime.start_hour_timetick integerValue]);
        [arrOriginalArr addObject:sTime.church_id];
    }
    // Initialise a new, empty mutable array
    NSMutableArray *unique = [NSMutableArray array];

    for (NSString * obj in arrOriginalArr) {
        if (![unique containsObject:obj]) {
            [unique addObject:obj];
        }
    }

    NSMutableArray *result = [NSMutableArray array];
    for (NSString * str in unique) {
        [result addObject:[self getChurchByIdx:str]];
    }
    if ([result count]) {
        self.searchResultViewController.arrResult = result;
        self.searchResultViewController.owner = self;
        [self.navigationController pushViewController:self.searchResultViewController animated:YES];
    }
    else{
        [Utility showAlert:APP_TITLE message:@"No result found!"];
    }
}

- (NSInteger) getTimetickByHourIndex:(NSInteger)index andDay:(NSString *)strDay{
    NSInteger timetick = (index)*60;
    if ([strDay isEqualToString:@"PM"]) {
        timetick += 12 * 60;
    }
    return timetick;
}

- (Church *) getChurchByIdx:(NSString *)strIdx{
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"idx == %@",strIdx];
    Church * church = [Church MR_findFirstWithPredicate:predicate];
    return church;
}

- (void)dismissAndSelectActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:1];
    [self.menu dismissWithClickedButtonIndex:1 animated:YES];
}

- (void)dismissAndCancelActivityActionSheet{
    [self actionSheet:self.menu clickedButtonAtIndex:0];
    [self.menu dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)goToDirectoryDetailFromSearchByServiceTimming:(SearchResultViewController *)viewController andChurch:(Church *)obj{
    [owner goToDirectoryDetailFromSearchByServiceTimming:self andChurch:obj];
}


@end
