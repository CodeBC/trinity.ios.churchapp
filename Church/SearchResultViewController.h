//
//  SearchResultViewController.h
//  Church
//
//  Created by Zayar on 5/22/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Church.h"
@protocol SearchResultViewControllerDelegate;
@interface SearchResultViewController : UIViewController
@property (nonatomic, strong) NSArray * arrResult;
@property (nonatomic) id<SearchResultViewControllerDelegate> owner;
@end
@protocol SearchResultViewControllerDelegate
- (void)goToDirectoryDetailFromSearchByServiceTimming:(SearchResultViewController *)viewController andChurch:(Church *)obj;
@end
