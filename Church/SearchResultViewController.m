//
//  SearchResultViewController.m
//  Church
//
//  Created by Zayar on 5/22/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SWChurchTableViewCell.h"
#import <MessageUI/MessageUI.h>
#import "DirectoryDetailViewController.h"
@interface SearchResultViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (nonatomic,strong) MFMailComposeViewController * mailComposeViewController;
@property (nonatomic,strong) DirectoryDetailViewController * detailViewController;
@end

@implementation SearchResultViewController
@synthesize arrResult,owner;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"result count %d",[self.arrResult count]);

    [self reLoadTheView];
}

- (void) reLoadTheView{
    [self.tbl reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    SWChurchTableViewCell *cell = (SWChurchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSLog(@"UI table width %f",cell.frame.size.width);
    if (cell == nil) {
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        //[rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"8EC447"] icon:[UIImage imageNamed:@"Star_Unselect2"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"phone_icon"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"location-icon"] andTag:indexPath.row];
        [rightUtilityButtons addUtilityButtonWithColor:[UIColor colorWithHexString:@"51c4d4"] icon:[UIImage imageNamed:@"envelope_icon"] andTag:indexPath.row];
        
        cell = [[SWChurchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier height:71 leftUtilityButtons:leftUtilityButtons rightUtilityButtons:rightUtilityButtons];
        
    }
    cell.delegate = self;
    cell.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    Church * obj = [self.arrResult objectAtIndex:indexPath.row];
    //NSLog(@"home author id %@",obj.author_id);
    /* UIButton *button = [cell.rightUtilityButtons objectAtIndex:0];
     if ([obj.is_fav intValue]==1) {
     [button setImage:[UIImage imageNamed:@"748-heart-filled"] forState:UIControlStateNormal];
     }
     else if([obj.is_fav intValue]==1){
     [button setImage:[UIImage imageNamed:@"748-heart"] forState:UIControlStateNormal];
     }*/
    
    [cell loadTheView:obj];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (homeDetailViewController == nil) {
     homeDetailViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"detailViewController"];
     }
     Poems * obj = [arrPoems objectAtIndex:indexPath.row];
     homeDetailViewController.objPoem = obj;
     [self.navigationController pushViewController:homeDetailViewController animated:YES];
     //[self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrResult count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark - SWTableViewDelegate
- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swippableTableViewCell:(SWChurchTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    Church * obj = [arrResult objectAtIndex:cell.tag];
    switch (index) {
        case 0:
        {
            /*NSLog(@"More button was pressed and cell tag %d",cell.tag);
             Poems * obj = [self.arrPoems objectAtIndex:cell.tag];
             UIButton *button = [cell.rightUtilityButtons objectAtIndex:index];
             NSLog(@"obj fav %d",[obj.is_fav intValue]);
             //button.backgroundColor = color;
             AppDelegate * delegate = [[UIApplication sharedApplication]delegate];
             NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
             if ([obj.is_fav intValue] == 0) {
             [button setImage:[UIImage imageNamed:@"748-heart-filled"] forState:UIControlStateNormal];
             obj.is_fav = [NSNumber numberWithInt:1];
             [self.arrPoems replaceObjectAtIndex:cell.tag withObject:obj];
             [cell wayToOriginalScrollView];
             }
             else if ([obj.is_fav intValue] == 1){
             [button setImage:[UIImage imageNamed:@"748-heart"] forState:UIControlStateNormal];
             obj.is_fav = [NSNumber numberWithInt:0];
             [self.arrPoems replaceObjectAtIndex:cell.tag withObject:obj];
             [cell wayToOriginalScrollView];
             }
             [delegate updatePoem:obj withLocalContext:localContext];*/
            [self onCallWithChurch:obj];
            break;
        }
            
        case 1:
        {
            [self onLocationWithChurch:obj];
            break;
        }
        case 2:
        {
            [self showMailComposeWithChurch:obj];
            break;
        }
        default:
            break;
    }
}

- (void) onCallWithChurch:(Church *)obj{
    NSString * number = [NSString stringWithFormat:@"tel://%@", [obj.telephone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    NSLog(@"Calling %@", number);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: number]];
}

- (void) showMailComposeWithChurch:(Church *)obj{
    // Email Subject
    NSString *emailTitle = @"Church";
    // Email Content
    NSString *messageBody = @"Church mail!";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:obj.email];
    
    
    [self.mailComposeViewController setSubject:emailTitle];
    [self.mailComposeViewController setMessageBody:messageBody isHTML:NO];
    [self.mailComposeViewController setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) onLocationWithChurch:(Church *)obj{
    NSString* addr = nil;
    addr = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f&saddr=Current", [obj.lat floatValue],[obj.lon floatValue]];
    NSURL* url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didSelectedTheCell:(SWChurchTableViewCell *)cell{
    Church * obj = [self.arrResult objectAtIndex:cell.tag];
    //self.detailViewController.headerView = headerView;
    //self.detailViewController.objChurch = obj;
    //self.detailViewController.isFromOther = 1;
    //[self.navigationController pushViewController:self.detailViewController animated:YES];
    //[self onSelectDay:nil];
    [owner goToDirectoryDetailFromSearchByServiceTimming:self andChurch:obj];
}

@end
