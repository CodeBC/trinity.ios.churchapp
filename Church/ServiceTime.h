//
//  ServiceTime.h
//  Church
//
//  Created by Zayar on 5/22/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ServiceTime : NSManagedObject

@property (nonatomic, retain) NSString * church_id;
@property (nonatomic, retain) NSString * created_at;
@property (nonatomic, retain) NSString * day;
@property (nonatomic, retain) NSString * end_am_pm;
@property (nonatomic, retain) NSNumber * end_hour;
@property (nonatomic, retain) NSString * end_minute;
@property (nonatomic, retain) NSString * idx;
@property (nonatomic, retain) NSString * start_am_pm;
@property (nonatomic, retain) NSNumber * start_hour;
@property (nonatomic, retain) NSString * start_minute;
@property (nonatomic, retain) NSString * updated_at;
@property (nonatomic, retain) NSNumber * start_hour_timetick;
@property (nonatomic, retain) NSNumber * end_hour_timetick;
- (void) setStartHourTimetick;

- (void) setEndHourTimetick;
@end
