//
//  ServiceTime.m
//  Church
//
//  Created by Zayar on 5/22/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ServiceTime.h"


@implementation ServiceTime

@dynamic church_id;
@dynamic created_at;
@dynamic day;
@dynamic end_am_pm;
@dynamic end_hour;
@dynamic end_minute;
@dynamic idx;
@dynamic start_am_pm;
@dynamic start_hour;
@dynamic start_minute;
@dynamic updated_at;
@dynamic start_hour_timetick;
@dynamic end_hour_timetick;
- (void) setStartHourTimetick{
    /*NSDateFormatter * formmatter = [[NSDateFormatter alloc] init];
    [formmatter setDateFormat:@"HH:mm aa"];
    NSString * strHour = [NSString stringWithFormat:@"%d:%@ %@",[self.start_hour integerValue],self.start_minute,self.start_am_pm];
    NSDate * startTime = [formmatter dateFromString:strHour];
    NSInteger sTimetick = [startTime timeIntervalSince1970];*/
    NSInteger sTimetick = [self.start_hour integerValue]*60 + [self.start_minute integerValue];
    self.start_hour_timetick = [NSNumber numberWithInteger:sTimetick];
    NSLog(@"start time tick %d",[self.start_hour_timetick integerValue]);
}

- (void) setEndHourTimetick{
    NSInteger sTimetick = [self.end_hour integerValue]*60 + [self.end_minute integerValue];
    self.end_hour_timetick = [NSNumber numberWithInteger:sTimetick];
    NSLog(@"end time tick %d",[self.end_hour_timetick integerValue]);
}
@end
