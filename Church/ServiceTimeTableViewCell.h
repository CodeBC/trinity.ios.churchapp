//
//  ServiceTimeTableViewCell.h
//  Church
//
//  Created by Zayar on 5/20/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceTime.h"
@interface ServiceTimeTableViewCell : UITableViewCell
- (void) setStartAndEndTime:(NSString *)strName andDay:(NSString *)strDay;
- (void) setUpView;

+ (CGFloat)heightForCellWithPost:(NSString *)str;
@end
