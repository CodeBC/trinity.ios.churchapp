//
//  ServiceTimeTableViewCell.m
//  Church
//
//  Created by Zayar on 5/20/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ServiceTimeTableViewCell.h"
#import "ServiceTime.h"
#import "TTTAttributedLabel.h"
@interface ServiceTimeTableViewCell()
@property (nonatomic,retain) TTTAttributedLabel * lblStartTime;
@end
@implementation ServiceTimeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UILabel *)lblStartTime{
    if (!_lblStartTime) {
        _lblStartTime = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(5, 0, 226, 15)];
        _lblStartTime.backgroundColor = [UIColor clearColor];
        _lblStartTime.textColor = [UIColor darkTextColor];
        _lblStartTime.font = [UIFont systemFontOfSize:11];
        _lblStartTime.textAlignment = NSTextAlignmentLeft;
        _lblStartTime.numberOfLines =0;
    }
    return _lblStartTime;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUpView{
    [self addSubview:self.lblStartTime];
}

- (void) setStartAndEndTime:(NSString *)strName andDay:(NSString *)strDay{
    //self.lblStartTime.text = [NSString stringWithFormat:@"%d:%@%@ | %d:%@%@",[obj.start_hour integerValue],obj.start_minute,obj.start_am_pm,[obj.end_hour integerValue],obj.end_minute,obj.end_am_pm];
    
    CGRect frame = self.lblStartTime.frame;
    frame.size.height = [self heightForCellWithPureHeight:[NSString stringWithFormat:@"%@ %@",strDay,strName]];
    [self.lblStartTime setFrame:frame];
    
    [self.lblStartTime setText:[NSString stringWithFormat:@"%@ %@",strDay,strName] afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        NSRange colorRange = [[mutableAttributedString string] rangeOfString:strDay options:NSCaseInsensitiveSearch];
        
        // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
        
        if ([strDay isEqualToString:@"Sat"] || [strDay isEqualToString:@"Sun"]) {
            [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[UIColor redColor].CGColor range:colorRange];
        }
        else{
            [mutableAttributedString addAttribute:(NSString *)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:colorRange];
        }
        
        
        return mutableAttributedString;
    }];
    
    //self.lblStartTime.text = [NSString stringWithFormat:@"%@ %@",strDay,strName];
    
}

+ (CGFloat)heightForCellWithPost:(NSString *)str {
    
    CGSize sizeToFit = [str sizeWithFont:[UIFont systemFontOfSize:11] constrainedToSize:CGSizeMake(226.0f, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    NSLog(@"size to fix %f",sizeToFit.height + 81.0f);
    return fmaxf(15.0f, sizeToFit.height+ 5);
}

- (CGFloat)heightForCellWithPureHeight:(NSString *)str {
    CGSize sizeToFit = [str sizeWithFont:[UIFont systemFontOfSize:11] constrainedToSize:CGSizeMake(226.0f, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    return fmaxf(15.0f, sizeToFit.height+5);
}

@end
