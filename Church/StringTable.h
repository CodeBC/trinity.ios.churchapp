//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}

extern NSString * const APP_TITLE;
extern NSString * const APP_ID;
extern NSString * const APP_SHARE_LINK;
extern NSString * const APP_BASED_LINK;
extern NSString * const CHURCH_LINK;
extern NSString * const MESSAGE_LINK;
extern NSString * const NEAR_BY_LINK;
extern CGFloat const MENU_BTN_WIDTH;
extern CGFloat const MENU_BTN_HEIGHT;
extern CGFloat const HEADER_GAP_HEIGHT;
extern CGFloat const SEARCH_GAP_HEIGHT;
extern CGFloat const ABOUT_IMG_HEIGHT;
extern CGFloat const MSG_SHARE_BTN_HEIGHT;
extern NSString * const BLANCE_CONDENSED;
extern NSString * const BLANCE_CAPS_INLINE;
extern NSString * const BLANCE_CAPS_LIGHT;
extern NSString * const APP_DB;
extern NSString * const ROBOTO_LIGHT;
extern NSString * const SERVICE_MONDAY;
extern NSString * const SERVICE_TUESDAY;
extern NSString * const SERVICE_WEDNESDAY;
extern NSString * const SERVICE_THURSDAY;
extern NSString * const SERVICE_FRIDAY;
extern NSString * const SERVICE_SATURDAY;
extern NSString * const SERVICE_SUNDAY;

extern float const ACTIONSHEET_HEIGHT;
@end
