//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable
NSString * const APP_DB = @"church.sqlite";
NSString * const APP_TITLE = @"Church";
NSString * const APP_ID = @"---";
NSString * const APP_SHARE_LINK = @"http://mschurch.herokuapp.com/";
NSString * const APP_BASED_LINK = @"http://mschurch.herokuapp.com/";//http://192.168.200.133/church/message.json
//NSString * const APP_BASED_LINK = @"http://192.168.200.133/";
NSString * const CHURCH_LINK = @"api/churches";
NSString * const MESSAGE_LINK = @"api/messages";
//NSString * const MESSAGE_LINK = @"church/message.json";
NSString * const NEAR_BY_LINK = @"api/churches/nearbys";
CGFloat const MENU_BTN_WIDTH=203;
CGFloat const MENU_BTN_HEIGHT=50;
CGFloat const HEADER_GAP_HEIGHT = 112;
CGFloat const SEARCH_GAP_HEIGHT = 66;
CGFloat const ABOUT_IMG_HEIGHT = 274;
CGFloat const MSG_SHARE_BTN_HEIGHT = 35;

NSString * const BLANCE_CONDENSED= @"Blanch-Condensed";
NSString * const BLANCE_CAPS_INLINE= @"Blanch-CapsInline";
NSString * const BLANCE_CAPS_LIGHT= @"Blanch-CapsLight";
NSString * const ROBOTO_LIGHT= @"Roboto-Light";

NSString * const SERVICE_MONDAY= @"monday";
NSString * const SERVICE_TUESDAY= @"tuesday";
NSString * const SERVICE_WEDNESDAY= @"wednesday";
NSString * const SERVICE_THURSDAY= @"thursday";
NSString * const SERVICE_FRIDAY= @"friday";
NSString * const SERVICE_SATURDAY= @"saturday";
NSString * const SERVICE_SUNDAY= @"sunday";

float const ACTIONSHEET_HEIGHT = 462;
@end
