//
//  ChurchAPIClient.h
//  Church
//
//  Created by Zayar on 5/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"

@interface ChurchAPIClient : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;
@end
