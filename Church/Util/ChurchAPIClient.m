//
//  ChurchAPIClient.m
//  Church
//
//  Created by Zayar on 5/2/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ChurchAPIClient.h"
#import "StringTable.h"
@implementation ChurchAPIClient

+ (instancetype)sharedClient {
    static ChurchAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ChurchAPIClient alloc] initWithBaseURL:[NSURL URLWithString:APP_BASED_LINK]];
        [_sharedClient setSecurityPolicy:[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey]];
    });
    
    return _sharedClient;
}
@end
