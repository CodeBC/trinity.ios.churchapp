//
//  AboutViewController.h
//  Church
//
//  Created by Zayar on 4/30/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
@protocol AboutViewControllerDelegate;
@interface AboutViewController : UIViewController
@property (nonatomic, strong) HeaderView * headerView;
@property (nonatomic) id<AboutViewControllerDelegate> owner;
- (void) reLoadTheView;
@end

@protocol AboutViewControllerDelegate
- (void) onHeaderViewTap:(AboutViewController *)viewController andHeaderView:(HeaderView *)headerView;
- (void) onAboutViewClose:(AboutViewController *)viewController;
@end
