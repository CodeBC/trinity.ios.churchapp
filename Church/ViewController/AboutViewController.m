//
//  AboutViewController.m
//  Church
//
//  Created by Zayar on 4/30/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AboutViewController.h"
#import "StringTable.h"
@interface AboutViewController ()
@property (nonatomic,strong) UIImageView * imgAboutView;
@property (nonatomic,strong) UIImageView * imgLogo;
@property (nonatomic,strong) UIImageView * imgBgView;
@property (nonatomic,strong) UILabel * lblProducedBy;
@property (nonatomic,strong) UILabel * lblTitle1;
@property (nonatomic,strong) UILabel * lblTitle2;
@property (nonatomic,strong) UILabel * lblTitle3;
@property (nonatomic,strong) UILabel * lblTitle4;
@property (nonatomic,strong) UIButton * btnClose;
@end

@implementation AboutViewController
@synthesize headerView,owner;

- (UIButton *)btnClose{
    if (!_btnClose) {
        _btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnClose setImage:[UIImage imageNamed:@"arrow_down_indicator"] forState:UIControlStateNormal];
        [_btnClose setFrame:CGRectMake(285, 5, 30, 30)];
        [_btnClose addTarget:self action:@selector(onClose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnClose;
}
- (UIImageView *)imgAboutView{
    if (!_imgAboutView) {
        _imgAboutView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x-self.view.frame.size.width, self.view.frame.size.height- ABOUT_IMG_HEIGHT, self.view.frame.size.width, ABOUT_IMG_HEIGHT)];
        [_imgAboutView setImage:[UIImage imageNamed:@"about"]];
        [_imgAboutView setHidden:YES];
    }
    return _imgAboutView;
}

- (UIImageView *)imgLogo{
    if (!_imgLogo) {
        _imgLogo = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x-self.view.frame.size.width, self.view.frame.size.height- ABOUT_IMG_HEIGHT, self.view.frame.size.width, ABOUT_IMG_HEIGHT)];
        [_imgLogo setImage:[UIImage imageNamed:@"about"]];
    }
    return _imgAboutView;
}

- (UIImageView *)imgBgView{
    if (!_imgBgView) {
        _imgBgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [_imgBgView setImage:[UIImage imageNamed:@"about_bg"]];
    }
    return _imgBgView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.imgBgView];
    [self.view addSubview:self.imgAboutView];
    [self.view addSubview:self.btnClose];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"aceef1"]];
}

- (void)onClose:(UIButton *)sender{
    NSLog(@"on close!");
    [owner onAboutViewClose:self];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"it is again");
    
}

- (void) imgAnimationSetup{
    self.imgAboutView.hidden = NO;
    POPSpringAnimation *popOutAnimation = [POPSpringAnimation animation];
    popOutAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewFrame];
    popOutAnimation.toValue = [NSValue valueWithCGRect:CGRectMake(self.view.frame.origin.x, (self.view.frame.size.height-ABOUT_IMG_HEIGHT), self.view.frame.size.width, ABOUT_IMG_HEIGHT)];
    //popOutAnimation.velocity = @(1000);
    popOutAnimation.springBounciness = 0;
    popOutAnimation.springSpeed = 0.5;
    [self.imgAboutView pop_addAnimation:popOutAnimation forKey:@"DirectoryViewSlideIn"];
}

- (void)viewDidDisappear:(BOOL)animated{
    self.imgAboutView.hidden = YES;
}

- (void) onHeaderTapToClose:(HeaderView *)headerView{
    [owner onHeaderViewTap:self andHeaderView:headerView];
}

- (void) reLoadTheView{
    [self performSelector:@selector(imgAnimationSetup) withObject:nil afterDelay:1];
    
    [self.headerView setNameText:@"ABOUT"];
    [self.headerView showFadeInView];
    self.headerView.owner = self;
    [self.headerView arrowAnimationForDetail:NO];
    [self.headerView showSearchButton:NO];
}

@end
