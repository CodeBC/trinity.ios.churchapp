//
//  BasedViewController.h
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasedViewController : UIViewController
- (void) startAnimation;
- (void) stopAnimation;
- (void) onShare;
- (void) showMailCompose;
@end
