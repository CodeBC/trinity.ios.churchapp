//
//  BasedViewController.m
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedViewController.h"
#import "CircleView.h"
#import "Utility.h"
#import "Message.h"
#import "StringTable.h"
#import <MessageUI/MessageUI.h>
@interface BasedViewController ()
{
	BOOL isAnimationShouldStop;
}
@property (nonatomic, strong) UIImageView * bgImgView;
@property (nonatomic, strong) NSMutableArray * arrCircleViews;
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@property (nonatomic,strong) MFMailComposeViewController * mailComposeViewController;
@property (nonatomic,strong) UIImageView * imgBgView;
@end

@implementation BasedViewController

/*- (MFMailComposeViewController *) mailComposeViewController{
    if (!_mailComposeViewController) {
        _mailComposeViewController = [[MFMailComposeViewController alloc] init];
        
    }
    return _mailComposeViewController;
}*/

- (UIImageView *) imgBgView{
    if (!_imgBgView) {
        _imgBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
        [_imgBgView setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height)];
        
    }
    return _imgBgView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.imgBgView];
    [self loadTheCirclesView];
    [self.view setBackgroundColor:[UIColor blackColor]];
}

- (UIImageView *) bgImgView{
    if (!_bgImgView) {
        _bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    }
    return _bgImgView;
}

- (NSMutableArray *) arrCircleViews{
    if (!_arrCircleViews) {
        _arrCircleViews = [[NSMutableArray alloc] init];
    }
    return _arrCircleViews;
}

- (void) loadTheCirclesView{
    if (![self.arrCircleViews count]) {
        int totalCircle = 5;
        for (int i = 1; i <= totalCircle; i++) {
            CGFloat x = (CGFloat) (arc4random() % (int) self.view.bounds.size.width);
            CGFloat y = (CGFloat) (arc4random() % (int) self.view.bounds.size.height);
            CircleView * circleView = [[CircleView alloc] initWithFrame:CGRectMake(x, y, 150, 150)];
            [circleView setAlpha:0.5];
            [self.view addSubview:circleView];
            [self.arrCircleViews addObject:circleView];
            [self.view bringSubviewToFront:circleView];
        }
    }
}

- (void) startAnimation{
    [self animationLoop:@"" finished:0 context:nil];
}

- (void)registerEffectForView:(UIView *)aView depth:(CGFloat)depth
{
    if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
        UIInterpolatingMotionEffect *effectX;
        UIInterpolatingMotionEffect *effectY;
        effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                  type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        
        
        effectX.maximumRelativeValue = @(depth);
        effectX.minimumRelativeValue = @(-depth);
        effectY.maximumRelativeValue = @(depth);
        effectY.minimumRelativeValue = @(-depth);
        
        [aView addMotionEffect:effectX];
        [aView addMotionEffect:effectY];
    }
}

- (void)stopAnimation{
    for (CircleView * cView in self.arrCircleViews) {
        isAnimationShouldStop = TRUE;
        [cView.layer removeAllAnimations];
        [cView removeFromSuperview];
    }
}

-(void)animationLoop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if (!isAnimationShouldStop) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:20];
        // remove:
        //  [UIView setAnimationRepeatCount:1000];
        //  [UIView setAnimationRepeatAutoreverses:YES];
        for (CircleView * cView in self.arrCircleViews) {
            CGFloat x = (CGFloat) (arc4random() % (int) self.view.bounds.size.width);
            CGFloat y = (CGFloat) (arc4random() % (int) self.view.bounds.size.height);
            
            CGFloat a1 = (CGFloat) (arc4random() % (int) 2);
            CGPoint squarePostion = CGPointMake(x, y);
            cView.center = squarePostion;
            [cView setAlpha:a1];
        }
        // add:
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationLoop:finished:context:)];
        
        [UIView commitAnimations];
    }
}

- (void)onShare{
    NSLog(@"detail on share!!");
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n via Church App[%@]",APP_TITLE,APP_SHARE_LINK],[NSString stringWithFormat:@"Original Link:[%@]",APP_SHARE_LINK]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
}

- (void) showMailCompose{
    // Email Subject
    NSString *emailTitle = @"Church";
    // Email Content
    NSString *messageBody = @"Church mail!";
    // To address
   // NSArray *toRecipents = [NSArray arrayWithObject:obj.email];
    self.mailComposeViewController = [[MFMailComposeViewController alloc] init];
    self.mailComposeViewController.mailComposeDelegate = self;
    
    [self.mailComposeViewController setSubject:emailTitle];
    [self.mailComposeViewController setMessageBody:messageBody isHTML:NO];
    //[self.mailComposeViewController setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
