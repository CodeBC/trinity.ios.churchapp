//
//  DirectoryDetailViewController.h
//  Church
//
//  Created by Zayar on 5/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Church.h"
#import "DirectoryBasedViewController.h"
#import "HeaderView.h"
@interface DirectoryDetailViewController : DirectoryBasedViewController
@property (nonatomic,strong) Church * objChurch;
@property NSInteger isFromOther;
@end
