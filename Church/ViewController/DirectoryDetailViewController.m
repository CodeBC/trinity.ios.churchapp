//
//  DirectoryDetailViewController.m
//  Church
//
//  Created by Zayar on 5/3/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DirectoryDetailViewController.h"
#import "StringTable.h"
#import <MapKit/MapKit.h>
#import "Utility.h"
#import <MessageUI/MessageUI.h>
#import "ServiceTime.h"
#import "ServiceTimeTableViewCell.h"
#import "NSString+HTML.h"
#import "DisplayMap.h"
@interface DirectoryDetailViewController ()
{
    NSMutableArray * arrServiceTimes;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UITableView *tblService;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) MFMailComposeViewController * mailComposeViewController;
@end

@implementation DirectoryDetailViewController
@synthesize objChurch,isFromOther;

- (MFMailComposeViewController *) mailComposeViewController{
    if (!_mailComposeViewController) {
        _mailComposeViewController = [[MFMailComposeViewController alloc] init];
        _mailComposeViewController.mailComposeDelegate = self;
    }
    return _mailComposeViewController;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [Utility makeCornerRadius:self.btnCall andRadius:3];
    [Utility makeCornerRadius:self.btnEmail andRadius:3];
    
}
- (void)viewWillAppear:(BOOL)animated{
    
    [self loadTheView];
    [self loadTheViewWithChurch:objChurch];
}

- (void) loadTheView{
    NSLog(@"obj name %@, header view %@",objChurch.name,self.headerView);
    [self.headerView setNameText:objChurch.name];
    [self.headerView showFadeInView];
    self.headerView.owner = self;
    [self.headerView arrowAnimationForDetail:YES];
    [self.headerView showSearchButton:NO];
}

- (IBAction)onCall:(id)sender {
    NSString * number = [NSString stringWithFormat:@"tel://%@", [objChurch.telephone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    NSLog(@"Calling %@", number);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: number]];
}

- (IBAction)onEmail:(id)sender {
    // Email Subject
    NSString *emailTitle = @"Church";
    // Email Content
    NSString *messageBody = @"Church mail!";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:objChurch.email];
    
    [self.mailComposeViewController setSubject:emailTitle];
    [self.mailComposeViewController setMessageBody:messageBody isHTML:NO];
    [self.mailComposeViewController setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void) onHeaderTapToClose:(HeaderView *)headerView{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLayoutSubviews {
    //[self.scrollView setContentSize:CGSizeMake(320, self.btnCall.frame.origin.y + self.btnCall.frame.size.height+10)];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
    }else{
        [self.scrollView setContentSize:CGSizeMake(320, 500+44)];
    }
}

- (void)loadTheViewWithChurch:(Church *)obj{
    //[self.lblAddress setText:[objChurch.address stringByConvertingHTMLToPlainText]];
    NSLog(@"address detail %@",obj.address);
    NSError *err;
    self.lblAddress.attributedText =
    [[NSAttributedString alloc]
     initWithData: [objChurch.address dataUsingEncoding:NSUTF8StringEncoding]
     options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
     documentAttributes: nil
     error: &err];
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
    
    
    [self.btnCall setTitle:objChurch.telephone forState:UIControlStateNormal];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"church_id == %@",obj.idx];
    NSArray * arr = [ServiceTime MR_findAllWithPredicate:predicate];
    //NSLog(@"service count %d",[arr count]);
    [self createDicForServiceTime:arr andChurchId:obj.idx];
    [self.tblService reloadData];
    [self loadTheMapWith:obj];
    
    //[self.scrollView setContentSize:CGSizeMake(320, 20)];
    //[self.scrollView setBackgroundColor:[UIColor redColor]];
    NSLog(@"content size!!!! %f",self.btnCall.frame.origin.y+self.btnCall.frame.size.height + 10);
    //[self.view bringSubviewToFront:self.scrollView];
}

- (void)createDicForServiceTime:(NSArray *)arr andChurchId:(NSString *)strId{
    NSArray * arrMonday = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ AND day LIKE 'Monday'",strId]];
    NSLog(@"arr monday count %d",[arrMonday count]);
    
    NSArray * arrTuesday = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Tuesday'",strId,SERVICE_TUESDAY]];
    
    NSArray * arrWed = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Wednesday'",strId,SERVICE_WEDNESDAY]];;
    NSArray * arrThur = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Thursday'",strId,SERVICE_THURSDAY]];
    
    NSArray * arrFri = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Friday'",strId,SERVICE_FRIDAY]];
    
    NSArray * arrSat = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Saturday'",strId,SERVICE_SATURDAY]];
    
    NSArray * arrSun = [ServiceTime MR_findAllSortedBy:@"start_hour_timetick" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"church_id == %@ && day LIKE 'Sunday'",strId,SERVICE_SUNDAY]];
    
    NSLog(@"arr arrWed count %d",[arrWed count]);
    
    if ([arr count]) {
        arrServiceTimes = [[NSMutableArray alloc] init];
        
        if ([arrMonday count]) {
            NSString * title = @"Mon";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrMonday};
            [arrServiceTimes addObject:dic];
        }
        if ([arrTuesday count]) {
            NSString * title = @"Tues";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrTuesday};
            [arrServiceTimes addObject:dic];
        }
        if ([arrWed count]) {
            NSString * title = @"Wed";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrWed};
            [arrServiceTimes addObject:dic];
        }
        if ([arrThur count]) {
            NSString * title = @"Thurs";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrThur};
            [arrServiceTimes addObject:dic];
        }
        if ([arrFri count]) {
            NSString * title = @"Fri";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrFri};
            [arrServiceTimes addObject:dic];
        }
        if ([arrSat count]) {
            NSString * title = @"Sat";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrSat};
            [arrServiceTimes addObject:dic];
        }
        if ([arrSun count]) {
            NSString * title = @"Sun";
            NSDictionary * dic = @{@"title":title,@"arr_objs":arrSun};
            [arrServiceTimes addObject:dic];
        }
    }
    else{
        if ([arrServiceTimes count]) {
            [arrServiceTimes removeAllObjects];
            arrServiceTimes = nil;
        }
    }
    NSLog(@"section count %d",[arrServiceTimes count]);
}

- (NSArray *) getServiceTimeArrayFromArray:(NSArray *)arr{
    NSMutableArray * arrService=nil;
    if ([arr count]) {
        arrService = [[NSMutableArray alloc] init];
    }
    
    for (ServiceTime * obj in arr) {
        [arrService addObject:obj];
    }
    return (NSArray *)arrService;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ServiceCell";
	ServiceTimeTableViewCell *cell = (ServiceTimeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ServiceTimeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpView];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    //Service * obj = [arrCart objectAtIndex:indexPath.row];
    NSDictionary * dic = [arrServiceTimes objectAtIndex:indexPath.row];
    NSString * strDay = [dic objectForKey:@"title"];
    NSArray * arr = [dic objectForKey:@"arr_objs"];
    NSString * strStartDate = [self   getStartTimeFromArray:arr];
    //[cell setStartAndEndTime:obj];
    [cell setStartAndEndTime:strStartDate andDay:strDay];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * dic = [arrServiceTimes objectAtIndex:indexPath.section];
    NSArray * arr = [dic objectForKey:@"arr_objs"];
    NSString * strStartDate = [self   getStartTimeFromArray:arr];
    return [ServiceTimeTableViewCell heightForCellWithPost:strStartDate]+ 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (homeDetailViewController == nil) {
     homeDetailViewController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"detailViewController"];
     }
     Poems * obj = [arrPoems objectAtIndex:indexPath.row];
     homeDetailViewController.objPoem = obj;
     [self.navigationController pushViewController:homeDetailViewController animated:YES];
     //[self.navigationItem.backBarButtonItem setTintColor:[UIColor redColor]];*/
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    /*NSDictionary * dic = [arrServiceTimes objectAtIndex:section];
    NSArray * arr = [dic objectForKey:@"arr_objs"];*/
    return [arrServiceTimes count];
}

- (NSString *) getStartTimeFromArray:(NSArray *)arr{
    NSString * str = @"";
    NSInteger i = 0;
    for (ServiceTime * obj in arr) {
        //NSLog(@"obj start time tick %d and time: %@",[obj.start_hour_timetick integerValue],[NSString stringWithFormat:@"%d:%@%@",[obj.start_hour integerValue],obj.start_minute,obj.start_am_pm]);
        if (i == 0) {
            str = [str stringByAppendingString:[NSString stringWithFormat:@"%d:%@%@",[obj.start_hour integerValue],obj.start_minute,obj.start_am_pm]];
        }
        else{
            str = [str stringByAppendingString:[NSString stringWithFormat:@" | %d:%@%@",[obj.start_hour integerValue],obj.start_minute,obj.start_am_pm]];
        }
        
        i ++;
    }
    return str;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * v=[[UIView alloc] init];
    UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 236, 20)];
    lbl.textColor = [UIColor darkGrayColor];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont systemFontOfSize:13];
    //lbl.textAlignment = NSTextAlignmentCenter;
     NSDictionary * dic = [arrServiceTimes objectAtIndex:section];
    NSString * str = [dic objectForKey:@"title"];
    lbl.text = str;
    [v addSubview:lbl];
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}*/

//all about map
- (void)removeAllAnnotations {
    
    NSMutableArray *annotationsToRemove = [NSMutableArray arrayWithCapacity:[self.mapView.annotations count]];
    NSLog(@"self mapview count %d",[self.mapView.annotations count]);
    
    for (int i = 0; i < [self.mapView.annotations count]; i++) {
        NSLog(@"removepin......");
        //if ([[self.mapView.annotations objectAtIndex:i] isKindOfClass:[DisplayMap class]]) {
        NSLog(@"removepin2......");
        [annotationsToRemove addObject:[self.mapView.annotations objectAtIndex:i]];
        //}
    }
    
    [self.mapView removeAnnotations:annotationsToRemove];
}

- (void)setUserSelectedLocation:(float)lat andLong:(float)lon{
    [self removeAllAnnotations];
    
    if (lat != 0.0 && lon != 0.0) {
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        CLLocationCoordinate2D touchMapCoordinate = location.coordinate;
        
        DisplayMap *ann = [[DisplayMap alloc] init];
        //ann.canShowCallout=YES;
        //ann.animatesDrop=YES;
        ann.coordinate = touchMapCoordinate;
        [self.mapView addAnnotation:ann];
        /*MKMapRect zoomRect = MKMapRectMake(0, 0, 0.002, 0.002);
         for (id <MKAnnotation> annotation in self.mapView.annotations)
         {
         MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
         MKMapRect pointRect = MKMapRectMake(annotationPoint.x , annotationPoint.y , 0.002, 0.002);// 0.1, 0.1
         zoomRect = MKMapRectUnion(zoomRect, pointRect);
         }
         [self.mapView setVisibleMapRect:zoomRect animated:YES];*/
        //[self.mapView setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(-100, -50, -50, -50) animated:YES];
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(touchMapCoordinate, 1000, 1000);
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
        [self.mapView setRegion:adjustedRegion animated:YES];
        //[mapView setCenterCoordinate:myCoord zoomLevel:13 animated:YES];
    }
}

- (void) loadTheMapWith:(Church *)obj{
    NSLog(@"obj lat %f and lon %f",[obj.lat floatValue],[obj.lon floatValue]);
    [self setUserSelectedLocation:[obj.lat floatValue] andLong:[obj.lon floatValue]];
}

@end
