//
//  DirectorySearchViewController.h
//  Church
//
//  Created by Zayar on 5/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectoryBasedViewController.h"
@interface DirectorySearchViewController : DirectoryBasedViewController
@property (nonatomic, strong) HeaderView * headerView;
@end
