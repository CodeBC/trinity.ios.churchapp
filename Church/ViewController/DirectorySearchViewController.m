//
//  DirectorySearchViewController.m
//  Church
//
//  Created by Zayar on 5/6/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "DirectorySearchViewController.h"
#import "Utility.h"
#import "SearchByNameViewController.h"
#import "SearchByServiceTimmingViewController.h"
#import "StringTable.h"
#import "DirectoryDetailViewController.h"
#import "CustomSegmentControl.h"
#import "SearchByLocationViewController.h"
@interface DirectorySearchViewController ()
@property (strong, nonatomic) IBOutlet CustomSegmentControl *sgmBar;
@property (strong, nonatomic) IBOutlet UIButton *btnDaySelect;
@property (strong, nonatomic) IBOutlet UIButton *btnTimeSelect;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic,strong) DirectoryDetailViewController * detailViewController;

@property (strong, nonatomic) UINavigationController * searchByNameViewController;
@property (strong, nonatomic) UINavigationController * searchByLocationViewController;
@property (strong, nonatomic) UINavigationController * searchByServiceTimmingViewController;

@end

@implementation DirectorySearchViewController
@synthesize headerView;
- (DirectoryDetailViewController *) detailViewController{
    if (!_detailViewController) {
        _detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectoryDetail"];
    }
    return _detailViewController;
}

- (UINavigationController *) searchByNameViewController{
    if (!_searchByNameViewController) {
        _searchByNameViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchName"];
        _searchByNameViewController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height-SEARCH_GAP_HEIGHT);
    }
    return _searchByNameViewController;
}

- (UINavigationController *) searchByLocationViewController{
    if (!_searchByLocationViewController) {
        _searchByLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocation"];
        _searchByLocationViewController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height-SEARCH_GAP_HEIGHT);
    }
    return _searchByLocationViewController;
}

- (UINavigationController *) searchByServiceTimmingViewController{
    if (!_searchByServiceTimmingViewController) {
        _searchByServiceTimmingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchServiceTimming"];
        _searchByServiceTimmingViewController.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height-SEARCH_GAP_HEIGHT);
    }
    return _searchByServiceTimmingViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utility makeBorder:self.btnDaySelect andWidth:1 andColor:[UIColor colorWithHexString:@"4099a1"]];
    [Utility makeBorder:self.btnTimeSelect andWidth:1 andColor:[UIColor colorWithHexString:@"4099a1"]];
    
    [Utility makeCornerRadius:self.btnSearch andRadius:3];
    [Utility makeCornerRadius:self.btnDaySelect andRadius:3];
    [Utility makeCornerRadius:self.btnTimeSelect andRadius:3];
}

- (void) onHeaderTapToClose:(HeaderView *)headerView{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self loadTheView];
    self.sgmBar.selectedSegmentIndex = 0;
    [self onSgmBar:self.sgmBar];
}

- (void) loadTheView{
    [self.headerView setNameText:@"DIRECTORY SEARCH"];
    [self.headerView showFadeInView];
    self.headerView.owner = self;
    [self.headerView arrowAnimationForDetail:YES];
    [self.headerView showSearchButton:NO];
}

- (IBAction)onSgmBar:(UISegmentedControl *)sender {
    NSLog(@"sender tag %d",sender.selectedSegmentIndex);
    [self.view endEditing:YES];
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self addChildViewController:self.searchByNameViewController];
            [self.containerView addSubview:self.searchByNameViewController.view];
            CGRect newFrame = self.containerView.frame;
            newFrame.origin.x = 0;
            newFrame.origin.y = 0;
            if ([self.searchByNameViewController.viewControllers count]) {
                SearchByNameViewController * viewController = (SearchByNameViewController *)[self.searchByNameViewController.viewControllers objectAtIndex:0];
                viewController.owner = self;
                viewController.view.frame = newFrame;
                [viewController endSearchBarEditing];
                [viewController reLoadTheView];
            }
        
            [self.view bringSubviewToFront:self.containerView];
            [self willMoveToParentViewController:nil];
            break;
        case 1:
            [self addChildViewController:self.searchByLocationViewController];
            [self.containerView addSubview:self.searchByLocationViewController.view];
            CGRect newFrame1 = self.containerView.frame;
            newFrame1.origin.x = 0;
            newFrame1.origin.y = 0 ;
            if ([self.searchByLocationViewController.viewControllers count]) {
                SearchByLocationViewController * viewController3 = (SearchByLocationViewController *)[self.searchByLocationViewController.viewControllers objectAtIndex:0];
                viewController3.owner = self;
                viewController3.view.frame = newFrame1;
            }
            [self.view bringSubviewToFront:self.containerView];
            [self willMoveToParentViewController:nil];
            NSLog(@"here is location tag %d",sender.selectedSegmentIndex);
            break;
        case 2:
            [self addChildViewController:self.searchByServiceTimmingViewController];
            CGRect newFrame2 = self.containerView.frame;
            newFrame2.origin.x = 0;
            newFrame2.origin.y = 0;
            NSLog(@"containerView frame height: %f ",newFrame1.size.height);
            if ([self.searchByServiceTimmingViewController.viewControllers count]) {
                SearchByServiceTimmingViewController * viewController2 = (SearchByServiceTimmingViewController *)[self.searchByServiceTimmingViewController.viewControllers objectAtIndex:0];
                [viewController2 popTheViewController];
                viewController2.view.frame = newFrame2;
                viewController2.owner = self;
            }
            
            [self.containerView addSubview:self.searchByServiceTimmingViewController.view];
            [self.view bringSubviewToFront:self.containerView];
            
            [self willMoveToParentViewController:nil];
            break;
        default:
            NSLog(@"No option for: %d", [sender selectedSegmentIndex]);
            break;
    }
}

- (void) goToDirectoryDetailFromSearchByName:(SearchByNameViewController *)searchByNameViewController andChurch:(Church *)obj{
    NSLog(@"go to detaill!!!!");
    self.detailViewController.objChurch = obj;
    self.detailViewController.headerView = headerView;
    self.detailViewController.isFromOther = 1;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

- (void) goToDirectoryDetailFromSearchByServiceTimming:(SearchByServiceTimmingViewController *)searchByNameViewController andChurch:(Church *)obj{
    self.detailViewController.objChurch = obj;
    self.detailViewController.headerView = headerView;
    self.detailViewController.isFromOther = 1;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

- (void) goToDirectoryDetailFromSearchByLocation:(SearchByLocationViewController *)searchByLocationViewController andChurch:(Church *)obj{
    NSLog(@"location selected22!!");
    self.detailViewController.objChurch = obj;
    self.detailViewController.headerView = headerView;
    self.detailViewController.isFromOther = 1;
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

@end
