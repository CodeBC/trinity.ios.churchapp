//
//  GospelViewController.h
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GospelViewControllerDelegate;
@interface GospelViewController : UIViewController
@property (nonatomic) id<GospelViewControllerDelegate> owner;
- (void)unloadTheView;
- (void)reloadTheView;
@end
@protocol GospelViewControllerDelegate
- (void) onViewClose:(GospelViewController *)viewController;
@end