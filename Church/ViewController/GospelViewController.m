//
//  GospelViewController.m
//  Church
//
//  Created by Zayar on 4/29/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "GospelViewController.h"
#import "MessageView.h"
#import "GestureUtility.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
@interface GospelViewController ()
{
    NSArray * arrMessage;
    NSInteger currentPagingIndex1;
}
@property (nonatomic,strong) MessageView * messagView;
@property (nonatomic,strong) UIScrollView * msgScrollView;
@property (nonatomic,strong) UIPageControl * pageControl;
@property (nonatomic,strong) UIButton * btnClose;
@property (nonatomic, strong) UIActivityIndicatorView * activityIndicator;
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@property (nonatomic,strong) MFMailComposeViewController * mailComposeViewController;
@property (nonatomic,strong) UIButton * btnShare;

@end

@implementation GospelViewController
@synthesize owner;
/*- (MessageView *) messagView{
    if (!_messagView) {
        _messagView = [[MessageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [_messagView setUpViews];
    }
    return _messagView;
}*/
/*- (UIButton *) btnShare{
    if (!_btnShare) {
        _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnShare setFrame:CGRectMake(290, self.view.frame.size.height-40, 21, 28)];
        [_btnShare setImage:[UIImage imageNamed:@"share_white_icon"] forState:UIControlStateNormal];
        [_btnShare addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnShare;
}*/




- (UIPageControl *) pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-30, self.view.frame.size.width, 30)];
        [_pageControl setUserInteractionEnabled:NO];
        
    }
    return _pageControl;
}

- (UIButton *)btnClose{
    if (!_btnClose) {
        _btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnClose setImage:[UIImage imageNamed:@"arrow_down_indicator"] forState:UIControlStateNormal];
        [_btnClose setFrame:CGRectMake(285, 5, 30, 30)];
        [_btnClose addTarget:self action:@selector(onClose:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnClose;
}

- (UIScrollView *)msgScrollView{
    if (!_msgScrollView) {
        _msgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        _msgScrollView.pagingEnabled = YES;
        _msgScrollView.delegate = self;
    }
    return _msgScrollView;
}

- (UIActivityIndicatorView *) activityIndicator{
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicator.frame = CGRectMake(self.view.center.x-30, self.view.center.y-30, 60, 60);
        _activityIndicator.hidesWhenStopped = YES;
    }
    return _activityIndicator;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self startAnimation];
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"00000"]];
    [self.view addSubview:self.msgScrollView];
    [self.view addSubview:self.pageControl];
    [self.view addSubview:self.btnClose];
    [self.view addSubview:self.activityIndicator];
    [self.view addSubview:self.btnShare];
    //[GestureUtility setDragForViewY:self.view withBeyondView:nil];
    
    currentPagingIndex1 = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recieveMessageUpdate:) name:@"refreshMessage" object:nil];
}

- (void)onShare:(UIButton *)sender{
    //NSLog(@"detail on share!!");
    [self showMailCompose];
}

- (void) showMailCompose{
    // Email Subject
    NSString *emailTitle = @"Church";
    // Email Content
    NSString *messageBody = @"Church mail!";
    // To address
    // NSArray *toRecipents = [NSArray arrayWithObject:obj.email];
    self.mailComposeViewController = [[MFMailComposeViewController alloc] init];
    self.mailComposeViewController.mailComposeDelegate = self;
    
    [self.mailComposeViewController setSubject:emailTitle];
    [self.mailComposeViewController setMessageBody:messageBody isHTML:NO];
    //[self.mailComposeViewController setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)onClose:(UIButton *)sender{
    NSLog(@"on close!");
    [self viewToClose];
}

- (void)viewToClose{
    [owner onViewClose:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [self reloadTheView];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self unloadTheView];
}

- (void)recieveMessageUpdate:(NSNotificationCenter *)sender{
    if ([arrMessage count]== 0) {
        [self reloadTheView];
    }
}

- (void)reloadTheView{
    NSLog(@"gospel reloadTheView");
    arrMessage = [Message MR_findAllSortedBy:@"order_id" ascending:YES];
    currentPagingIndex1 = 0;
    if ([arrMessage count]==0) {
        [self.activityIndicator startAnimating];
    }
    else{
        [self.activityIndicator stopAnimating];
    }
    for(NSInteger i=0;i<arrMessage.count;i++){
        Message * obj = [arrMessage objectAtIndex:i];
        MessageView * messageView = [[MessageView alloc] initWithFrame:CGRectMake(self.msgScrollView.frame.size.width*i, 0, self.msgScrollView.frame.size.width, self.msgScrollView.frame.size.height)];
        [messageView loadTheViewWithMessage:obj];
        messageView.owner = self;
        messageView.tag = i;
        if (messageView.tag == currentPagingIndex1) {
            //[messageView setMainImageWithAnimation];
            
        }
        if ([obj.idx isEqualToString:@"6"]) {
            [messageView hideDarkOverlay];
        }
        [self.msgScrollView addSubview:messageView];
    }
    self.pageControl.numberOfPages = [arrMessage count];
    self.pageControl.currentPage = currentPagingIndex1;
    [self.msgScrollView setContentSize:CGSizeMake(self.msgScrollView.frame.size.width*[arrMessage count], self.msgScrollView.frame.size.height)];
    /*for (UIView * v in self.msgScrollView.subviews) {
        if ([v isKindOfClass:[MessageView class]]) {
            MessageView * messageView = (MessageView *)v;
            [messageView removeMainImage];
        }
    }*/
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
}

- (void)messageScroll:(UIScrollView *)msgScrollView andView:(MessageView *)msgView{
    NSLog(@"scroll view y %f",msgScrollView.contentOffset.y);
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if( page != currentPagingIndex1 ){
        for (UIView * v in scrollView.subviews) {
            if ([v isKindOfClass:[MessageView class]]) {
                MessageView * messageView = (MessageView *)v;
                if (messageView.tag == currentPagingIndex1) {
                    [messageView removeMainImage];
                }

                if (messageView.tag == page) {
                    [messageView setMainImageWithAnimation];
                }
            }
            
        }
        currentPagingIndex1 = page;
        self.pageControl.currentPage = currentPagingIndex1;
    }
}

- (void)onShareWithMessage:(Message *)objMsg andView:(MessageView *)msgView{
    NSLog(@"detail on share!!");
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n via Church App[%@]",objMsg.tittle,objMsg.image_url],[NSString stringWithFormat:@"Original Link:[%@]",objMsg.image_url]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
}

- (void)unloadTheView{
    for (UIView * v in self.msgScrollView.subviews) {
        if ([v isKindOfClass:[MessageView class]]) {
            MessageView * messageView = (MessageView *)v;
            if (messageView.tag == currentPagingIndex1) {
                [messageView unloadTheScroll];
            }
            
            [messageView removeFromSuperview];
            messageView = nil;
        }
        
    }
    NSLog(@"gospel unloadTheView");
    arrMessage = nil;
    [self.activityIndicator stopAnimating];
    [self.msgScrollView scrollRectToVisible:CGRectMake(0, 0, 320, self.msgScrollView.frame.size.height) animated:YES];
    currentPagingIndex1 = 0;
}

@end
